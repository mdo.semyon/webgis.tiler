﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebGIS.Tiler
{
    public class TileType
    {
        public int Zoom { get; set; }
        public int TileX { get; set; }
        public int TileY { get; set; }
    }
}

﻿namespace WebGIS.Tiler.Logging
{
    public class QueueRecordID
    {
        public long QueueID { get; set; }
        public int EntityID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebGIS.Core.DatabaseCore;
using WebGIS.Tiler.DL.Enums;
using WebGIS.Tiler.Renderers;
using WebGIS.Tiler.Repository;
using WebGIS.TileRendering.Conversions;

namespace WebGIS.Tiler.Logging
{ 
    public class LogManager
    {
        private static string _csWebGIS;
        private static string WebGIS_OUT
        {
            get
            {
                if (string.IsNullOrEmpty(LogManager._csWebGIS))
                {
                    var css = ConfigurationManager.ConnectionStrings["WebGIS_OUT"];

                    if (css == null || string.IsNullOrEmpty(css.ConnectionString))
                    {
                        Environment.Exit(1);
                    }
                    else
                    {
                        LogManager._csWebGIS = css.ConnectionString;
                    }
                }

                return LogManager._csWebGIS;
            }
        }

        public static bool GenerationResult { get; set; }
        public static int RunID { get; set; }

        public static void LogCommon(string eventSubject, 
                                    string eventText,
                                    ActionTypeEnum action, 
                                    SeverityEnum severity = SeverityEnum.Common)
        {
            if (severity == SeverityEnum.Error)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("{0}. {1} {2}", DateTime.Now, eventSubject, string.IsNullOrWhiteSpace(eventText) ? "" : eventText));
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else
            {
                Console.WriteLine(string.Format("{0}. {1} {2}", DateTime.Now, eventSubject, string.IsNullOrWhiteSpace(eventText) ? "" : eventText));
            }

            using (var dbContext = new DBContext(new DBConnectionType(LogManager.WebGIS_OUT)))
            {
                using (var command = dbContext.CreateCommand("[tiler].[LogEvent]"))
                {
                    command.AddParameter("@runID", SqlDbType.Int, RunID);
                    command.AddParameter("@eventSubject", SqlDbType.NVarChar, eventSubject);
                    command.AddParameter("@eventText", SqlDbType.NVarChar, eventText);
                    command.AddParameter("@severity", SqlDbType.Int, severity);
                    command.AddParameter("@actionType", SqlDbType.Int, action);
                    command.AddParameter("@objectTypeID", SqlDbType.Int, Renderer.ObjectTypeID);
                    command.AddParameter("@geoSectionObjectID", SqlDbType.Int, Renderer.GeoSectionObjectID);
                    command.ExecuteNonQuery();
                }
            }
        }

        public static void LogError(string eventSubject,
                                    string eventText,
                                    int zoom,
                                    int tileX,
                                    int tileY,
                                    ActionTypeEnum action,
                                    SeverityEnum severity = SeverityEnum.Common)
        {
            string quadkey = Helpers.TileXYToQuadKey(tileX, tileY, zoom);

            if (!string.IsNullOrEmpty(eventText) && eventText.ToLower().Contains("outofmemory"))
            {
                eventText = eventText + "- памяти не хватает… мы хотим обнять небо, но никогда не сможем...";
            }

            if (severity == SeverityEnum.Error)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("{0}. {1} {2}", DateTime.Now, eventSubject, string.IsNullOrWhiteSpace(eventText) ? "" : eventText));
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else
            {
                Console.WriteLine(string.Format("{0}. {1} {2}", DateTime.Now, eventSubject, string.IsNullOrWhiteSpace(eventText) ? "" : eventText));
            }

            using (var dbContext = new DBContext(new DBConnectionType(LogManager.WebGIS_OUT)))
            {
                using (var command = dbContext.CreateCommand("[tiler].[LogEvent]"))
                {
                    command.AddParameter("@runID", SqlDbType.Int, RunID);
                    command.AddParameter("@eventSubject", SqlDbType.NVarChar, eventSubject);
                    command.AddParameter("@eventText", SqlDbType.NVarChar, eventText);
                    command.AddParameter("@quadkey", SqlDbType.NVarChar, quadkey);
                    command.AddParameter("@severity", SqlDbType.Int, severity);
                    command.AddParameter("@actionType", SqlDbType.Int, action);
                    command.AddParameter("@objectTypeID", SqlDbType.Int, Renderer.ObjectTypeID);
                    command.AddParameter("@geoSectionObjectID", SqlDbType.Int, Renderer.GeoSectionObjectID);
                    command.ExecuteNonQuery();
                }
            }
        }

        public static void LogError(string eventSubject,
                                    string eventText,
                                    string quadkey,
                                    ActionTypeEnum action,
                                    SeverityEnum severity = SeverityEnum.Common)
        {
            if (severity == SeverityEnum.Error)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("{0}. {1} {2}", DateTime.Now, eventSubject, string.IsNullOrWhiteSpace(eventText) ? "" : eventText));
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else
            {
                Console.WriteLine(string.Format("{0}. {1} {2}", DateTime.Now, eventSubject, string.IsNullOrWhiteSpace(eventText) ? "" : eventText));
            }

            using (var dbContext = new DBContext(new DBConnectionType(LogManager.WebGIS_OUT)))
            {
                using (var command = dbContext.CreateCommand("[tiler].[LogEvent]"))
                {
                    command.AddParameter("@runID", SqlDbType.Int, RunID);
                    command.AddParameter("@eventSubject", SqlDbType.NVarChar, eventSubject);
                    command.AddParameter("@eventText", SqlDbType.NVarChar, eventText);
                    command.AddParameter("@quadkey", SqlDbType.NVarChar, quadkey);
                    command.AddParameter("@severity", SqlDbType.Int, severity);
                    command.AddParameter("@actionType", SqlDbType.Int, action);
                    command.AddParameter("@objectTypeID", SqlDbType.Int, Renderer.ObjectTypeID);
                    command.AddParameter("@geoSectionObjectID", SqlDbType.Int, Renderer.GeoSectionObjectID);
                    command.ExecuteNonQuery();
                }
            }
        }

        public static void DeleteOldLog()
        {
            using (var dbContext = new DBContext(new DBConnectionType(LogManager.WebGIS_OUT)))
            {
                using (var command = dbContext.CreateCommand("tiler.TruncateGenerationEventLog"))
                {
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}

﻿using System;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;

namespace WebGIS.Tiler
{
    public class TilerConfig
    {
        public string WebGIS_IN { get; set; }
        public string WebGIS_OUT { get; set; }
        public string OutputFolder { get; set; }
        public int BigPolygonSize { get; set; }
        public int BigMultipolygonSize { get; set; }
        public int MeshLevel { get; set; }
        public bool ConvertTo8Bit { get; set; }
        public int EmptyTileSize { get; set; }
        public int MaxConnectionsCount { get; set; }
        public int MaxWorkingThreads { get; set; }
        public int AffinityMask { get; set; }
        public int BunchSize { get; set; }
        public int MinZoom { get; set; }
        public int MaxZoom { get; set; }

        public TilerConfig()
        {
        }

        public static TilerConfig Read()
        {
            TilerConfig res = new TilerConfig();

            if (ConfigurationManager.AppSettings["OutputFolder"] != null)
            {
                res.OutputFolder = ConfigurationManager.AppSettings["OutputFolder"];

                if (!string.IsNullOrWhiteSpace(res.OutputFolder) && IsValidPathname(res.OutputFolder))
                {
                    res.OutputFolder = res.OutputFolder.TrimEnd(Path.DirectorySeparatorChar);
                    res.OutputFolder = res.OutputFolder.TrimEnd('/');
                }
                else
                {
                    res = null;
                    throw new ArgumentNullException("configuration", "Параметр OutputFolder имеет неверный формат.");
                }
            }

            int tryParseValue;
            if (ConfigurationManager.AppSettings["BigPolygonSize"] != null && int.TryParse(ConfigurationManager.AppSettings["BigPolygonSize"], out tryParseValue))
            {
                res.BigPolygonSize = tryParseValue;
            }
            else
            {
                res = null;
                throw new ArgumentNullException("configuration", "Параметр BigPolygonSize имеет неверный формат.");
            }

            if (ConfigurationManager.AppSettings["BigMultipolygonSize"] != null && int.TryParse(ConfigurationManager.AppSettings["BigMultipolygonSize"], out tryParseValue))
            {
                res.BigMultipolygonSize = tryParseValue;
            }
            else
            {
                res = null;
                throw new ArgumentNullException("configuration", "Параметр BigMultipolygonSize имеет неверный формат.");
            }

            if (ConfigurationManager.AppSettings["MeshLevel"] != null && int.TryParse(ConfigurationManager.AppSettings["MeshLevel"], out tryParseValue))
            {
                res.MeshLevel = tryParseValue;
            }
            else
            {
                res = null;
                throw new ArgumentNullException("configuration", "Параметр MeshLevel имеет неверный формат.");
            }

            if (ConfigurationManager.AppSettings["EmptyTileSize"] != null && int.TryParse(ConfigurationManager.AppSettings["EmptyTileSize"], out tryParseValue))
            {
                res.EmptyTileSize = tryParseValue;
            }
            else
            {
                res = null;
                throw new ArgumentNullException("configuration", "Параметр EmptyTileSize имеет неверный формат.");
            }

            if (ConfigurationManager.AppSettings["MaxConnectionsCount"] != null && int.TryParse(ConfigurationManager.AppSettings["MaxConnectionsCount"], out tryParseValue))
            {
                res.MaxConnectionsCount = tryParseValue;
            }
            else
            {
                res = null;
                throw new ArgumentNullException("configuration", "Параметр MaxConnectionsCount имеет неверный формат.");
            }

            if (ConfigurationManager.AppSettings["MaxWorkingThreads"] != null && int.TryParse(ConfigurationManager.AppSettings["MaxWorkingThreads"], out tryParseValue))
            {
                res.MaxWorkingThreads = tryParseValue;
            }
            else
            {
                res = null;
                throw new ArgumentNullException("configuration", "Параметр MaxWorkingThreads имеет неверный формат.");
            }

            if (ConfigurationManager.AppSettings["AffinityMask"] != null && int.TryParse(ConfigurationManager.AppSettings["AffinityMask"], out tryParseValue))
            {
                res.AffinityMask = tryParseValue;
            }
            else
            {
                res = null;
                throw new ArgumentNullException("configuration", "Параметр AffinityMask имеет неверный формат.");
            }

            if (ConfigurationManager.AppSettings["BunchSize"] != null && int.TryParse(ConfigurationManager.AppSettings["BunchSize"], out tryParseValue))
            {
                res.BunchSize = tryParseValue;
            }
            else
            {
                res = null;
                throw new ArgumentNullException("configuration", "Параметр BunchSize имеет неверный формат.");
            }

            if (ConfigurationManager.AppSettings["MinZoom"] != null && int.TryParse(ConfigurationManager.AppSettings["MinZoom"], out tryParseValue))
            {
                res.MinZoom = tryParseValue;
            }
            else
            {
                res = null;
                throw new ArgumentNullException("configuration", "Параметр MinZoom имеет неверный формат.");
            }

            if (ConfigurationManager.AppSettings["MaxZoom"] != null && int.TryParse(ConfigurationManager.AppSettings["MaxZoom"], out tryParseValue))
            {
                res.MaxZoom = tryParseValue;
            }
            else
            {
                res = null;
                throw new ArgumentNullException("configuration", "Параметр MaxZoom имеет неверный формат.");
            }


            bool boolTryParseValue;
            if (ConfigurationManager.AppSettings["ConvertTo8Bit"] != null && Boolean.TryParse(ConfigurationManager.AppSettings["ConvertTo8Bit"], out boolTryParseValue))
            {
                res.ConvertTo8Bit = boolTryParseValue;
            }
            else
            {
                res = null;
                throw new ArgumentNullException("configuration", "Параметр ConvertTo8Bit имеет неверный формат.");
            }

            if (ConfigurationManager.ConnectionStrings["WebGIS_IN"] != null && !string.IsNullOrEmpty(ConfigurationManager.ConnectionStrings["WebGIS_IN"].ConnectionString))
            {
                res.WebGIS_IN = ConfigurationManager.ConnectionStrings["WebGIS_IN"].ConnectionString;
            }
            else
            {
                res = null;
                throw new ArgumentNullException("configuration", "Параметр WebGIS_IN имеет неверный формат.");
            }

            if (ConfigurationManager.ConnectionStrings["WebGIS_OUT"] != null && !string.IsNullOrEmpty(ConfigurationManager.ConnectionStrings["WebGIS_OUT"].ConnectionString))
            {
                res.WebGIS_OUT = ConfigurationManager.ConnectionStrings["WebGIS_OUT"].ConnectionString;
            }
            else
            {
                res = null;
                throw new ArgumentNullException("configuration", "Параметр WebGIS_OUT имеет неверный формат.");
            }

            return res;
        }

        private static bool IsValidPathname(string testName)
        {
            Regex containsABadCharacter = new Regex("[" + Regex.Escape(new string(System.IO.Path.GetInvalidPathChars())) + "]");
            if (containsABadCharacter.IsMatch(testName)) { return false; };

            // other checks for UNC, drive-path format, etc

            return true;
        }
    }
}

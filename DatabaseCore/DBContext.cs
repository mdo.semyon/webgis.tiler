﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;

namespace WebGIS.Core.DatabaseCore
{
	/// <summary>
	/// Предназначен для создания соединения с базой данных
	/// </summary>
	public class DBContext : IDisposable, IDBConnectType
	{
		#region Fields

		/// <summary>
		/// Экземпляр класса, предоставляющего интерфейс доступа к взаимодействию с базой данных
		/// </summary>
		IConnection _connection;

		/// <summary>
		/// Было ли открыто соединение в внутри данного класса или его отрыли извне и передали внутрь класса
		/// </summary>
		readonly bool _isInnerConnection;

		/// <summary>
		/// Были ли освобожденый ресурсы, занятые классом
		/// </summary>
		bool _isDisposed;

		/// <summary>
		/// Была ли открыта транзакция в экземпляре этого класса, или транзакция была открыта другими экземплярами данного класса
		/// </summary>
		bool _isInnerTransaction;

		#endregion

        #region Properties

        /// <summary>
        /// Строка соединения с базой данных
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return _connection.ConnectionString;
            }
        }

        /// <summary>
        /// Экземпляр класса, предоставляющего интерфейс доступа к взаимодействию с базой данных
        /// </summary>
        public IConnection Connection
        {
            get
            {
                return _connection;
            }
        }

        public ConnectionState State
		{
			get
			{
				return _connection.SqlConnection.State;
			}
		}

        #endregion

        #region Constructor&Destructor

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="dbConnectionType">Параметры соединения</param>
        /// <param name="objectTypeID">Идентификатор типа объекта (для распределения нагрузки по нескольким БД)</param>
	    public DBContext(IDBConnectType dbConnectionType, int? objectTypeID = null)
	    {
            Debug.Assert(dbConnectionType != null, "Передан неинициализированный экземпляр класса параметров соеденения с БД");

            _isInnerConnection = false;
            _isInnerTransaction = false;
            _isDisposed = false;
            _connection = null;

            if (!(dbConnectionType is DBContext))
            {
                if (dbConnectionType.Connection == null)
                {
                    _connection = dbConnectionType.CreateConnection();
#if DEBUG
                    new SqlConnectionDebug(_connection);
#endif
                    _isInnerConnection = true;

                    _connection.Open();
                }
                else
                {
                    _connection = dbConnectionType.Connection;
                }

                if (_connection.State != ConnectionState.Open)
                {
                    throw new Exception("Не удалось открыть соеденение");
                }
            }
            else
            {
                _connection = ((DBContext)dbConnectionType)._connection;
            }

            if (objectTypeID.HasValue)
            {
                using (var cmd = CreateCommand("[lb].[GetLoadBalancingDBCS]"))
                {
                    cmd.AddParameter("@objectTypeID", SqlDbType.Int, objectTypeID);

                    var lbCS = cmd.ExecuteScalar() as string;

                    if (!string.IsNullOrWhiteSpace(lbCS))
                    {
                        if (_isInnerConnection)
                        {
                            _connection.Close();
                        }

                        _connection = dbConnectionType.CreateConnection(lbCS);
#if DEBUG
                        new SqlConnectionDebug(_connection);
#endif
                        _isInnerConnection = true;

                        _connection.Open();

                        if (_connection.State != ConnectionState.Open)
                        {
                            throw new Exception("Не удалось открыть соеденение");
                        }
                    }
                }
            }

            if (_isInnerConnection)
            {
                using (var cmd = CreateTextCommand("SET ARITHABORT ON;"))
                {
                    cmd.ExecuteNonQuery();
                }
            }
	    }

	    ~DBContext()
	    {
            Dispose(false);
        }

	    #endregion

		#region Methods

		public IConnection CreateConnection()
		{
			return _connection;
		}

	    public IConnection CreateConnection(string externalCS)
	    {
	        return CreateConnection();
	    }

	    /// <summary>
		/// Открытие транзакции
		/// </summary>
		public void BeginTransaction()
		{
			if (_connection.SqlTransaction == null)
			{
				_connection.BeginTransaction();
				_isInnerTransaction = true;
			}
		}

		/// <summary>
		/// Фиксация транзакции
		/// </summary>
		public void CommitTransaction()
		{
			if (_isInnerTransaction)
			{
				_connection.Commit();
			}
		}

		/// <summary>
		/// Создание объекта DbCommand для хранимой процедуры (инициализированного текущими соединением и транзакцией)
		/// </summary>
		/// <returns>Экземпляр созданного класса DbCommand</returns>
		public DbCommand CreateCommand(string spName)
		{
			var cmd = new SqlCommand
			{
			    CommandText = spName,
			    CommandType = CommandType.StoredProcedure,
			    Connection = _connection.SqlConnection,
			    Transaction = _connection.SqlTransaction
			};

		    return cmd;
		}

        /// <summary>
        /// Создание объекта DbCommand для текстового запроса (инициализированного текущими соединением и транзакцией)
        /// </summary>
        /// <returns>Экземпляр созданного класса DbCommand</returns>
        public DbCommand CreateTextCommand(string commandText)
        {
            var cmd = new SqlCommand
            {
                CommandText = commandText,
                CommandType = CommandType.Text,
                Connection = _connection.SqlConnection,
                Transaction = _connection.SqlTransaction
            };

            return cmd;
        }

		/// <summary>
		/// Закрытие соединения
		/// </summary>
		public void Close()
		{
			Dispose();
		}

        /// <summary>
        /// Освобождение памяти объекта
        /// </summary>
        private void Dispose(bool isDisposing)
        {
            if (!_isDisposed)
            {
                if (isDisposing)
                {
                    // Очищаем память управляемых ресурсов
                }

                // Очищаем память неуправляемых ресурсов

                if (_isInnerConnection)
                {
                    _connection.Close();
                }

                _connection = null;

                _isDisposed = true;
            }
        }

        /// <summary>
		/// Освобождение памяти объекта
		/// </summary>
		public void Dispose()
		{
            Dispose(true);

            GC.SuppressFinalize(this);
        }

		#endregion
	}
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;


namespace WebGIS.Core.DatabaseCore
{
	/// <summary>
	/// Класс - Предназначен для отладочных целей.
	/// </summary>
	public sealed class SqlConnectionDebug
	{
		#region fields
		/// <summary>
		/// Экземпляр класса предоставляющего интерфейс доступа к базе данных.
		/// </summary>
		IConnection _connection;
		/// <summary>
		/// Хеш таблица открытых соеденений
		/// </summary>
		static Hashtable _currentConnections = new Hashtable();
		#endregion

		#region properties
		#region ConnectionCount
		/// <summary>
		/// Get - Количество соеденения с базой данных.
		/// </summary>
		public static int ConnectionCount
		{
			get
			{
				return _currentConnections.Count;
			}
		}
		#endregion
		#endregion

		#region constructor
		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="conn">Экземпля класса CConnection.</param>
		public SqlConnectionDebug(IConnection conn)
		{
			if (conn.SqlConnection == null)
			{
				throw new Exception("Не правильный вызов отладочного класса. Свойство conn у класса CConnection должно быть проинициализировано.");
			}
			_connection = conn;
			_connection.SqlConnection.StateChange += new StateChangeEventHandler(conn_StateChange);
		}
		#endregion

		#region methods
		#region Unload(int threadID)
		/// <summary>
		/// Проверка перед закрытием потока, не осталось ли в нем открытых соеденений.
		/// </summary>
		/// <param name="threadID">Идентификатор потока проверяемого на оставшиеся в нем открытые соеденения.</param>
		public static void Unload(int threadID)
		{
			lock (_currentConnections)
			{
				if (_currentConnections.Count > 0)
				{
					IDictionaryEnumerator i = _currentConnections.GetEnumerator();
					IConnection conn = null;
					while (i.MoveNext())
					{
						if (i.Key.ToString().Contains("__" + threadID.ToString()))
						{
							if ((i.Value as IConnection).State == ConnectionState.Open)
							{
								conn = (i.Value as IConnection);
							}
						}
					}
					if (conn != null)
					{
						_currentConnections.Remove(conn.ConnectionString);
						conn.Close();
						throw new Exception("Вы забыли закрыть соденение с базой данных.\r\n\r\n\r\nStack Info:\r\n" + System.Environment.StackTrace);
					}
				}
			}
		}
		#endregion

		#region conn_StateChange(object sender, StateChangeEventArgs e)
		/// <summary>
		/// Обработчик изменения состояния соеденения с базой данных.
		/// </summary>
		private void conn_StateChange(object sender, StateChangeEventArgs e)
		{
			if (e.CurrentState == ConnectionState.Open)
			{
				if (_connection.SqlConnection.State == ConnectionState.Open)
				{
					if (_currentConnections.ContainsKey(_connection.ConnectionString + "__" + Thread.CurrentThread.ManagedThreadId.ToString()))
					{
						if ((_currentConnections[_connection.ConnectionString + "__" + Thread.CurrentThread.ManagedThreadId.ToString()] as IConnection).State == ConnectionState.Open)
						{
							_connection.SqlConnection.Close();
							throw new Exception("Вы пытаетесь открыть два соеденения в одном потоке к одной базе данных.\r\n\r\n\r\nStack Info:\r\n" + System.Environment.StackTrace);
						}
					}
					else
					{
						_currentConnections.Add(_connection.ConnectionString + "__" + Thread.CurrentThread.ManagedThreadId.ToString(), _connection);
					}
				}
			}
			if (e.CurrentState == ConnectionState.Closed)
			{
				if (_currentConnections.ContainsKey(_connection.ConnectionString + "__" + Thread.CurrentThread.ManagedThreadId.ToString()))
				{
					_currentConnections.Remove(_connection.ConnectionString + "__" + Thread.CurrentThread.ManagedThreadId.ToString());
				}
			}
		}
		#endregion
		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebGIS.Core.DatabaseCore
{
	public interface IDBConnectType
	{
		string ConnectionString { get; }
		IConnection Connection { get; }
		IConnection CreateConnection();
        IConnection CreateConnection(string externalCS);
	}
}

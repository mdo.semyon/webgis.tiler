﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGIS.Core.DatabaseCore;

namespace WebGIS.Core.DatabaseCore
{
	public class DBConnectionType : IDBConnectType
	{
		public string ConnectionString { get; private set; }

		public IConnection Connection { get; private set; }

		public IConnection CreateConnection()
		{
		     return CreateConnection(null);
		}

	    public IConnection CreateConnection(string externalCS)
	    {
	        if (!string.IsNullOrWhiteSpace(externalCS))
	        {
                return new DBConnection(externalCS);
	        }
	        else
	        {
                if (!string.IsNullOrEmpty(ConnectionString))
                {
                    return new DBConnection(ConnectionString);
                }
                else
                {
                    return Connection;
                }
            }
	    }

	    public DBConnectionType(string connectionString)
		{
			ConnectionString = connectionString;
		}

        public DBConnectionType(DBConnection connection)
		{
			Connection = connection;
		}
	}
}

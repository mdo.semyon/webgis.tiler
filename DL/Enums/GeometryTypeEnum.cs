﻿namespace WebGIS.Tiler.DL.Enums
{
    public enum GeometryTypeEnum
    {
        Point = 1,
        Polyline = 2,
        Sector = 3,
        Polygon = 4,
        Multipolygon = 5,
        All = 6
    }
}
 
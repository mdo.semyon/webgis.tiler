﻿namespace WebGIS.Tiler.DL.Enums
{
    public enum ActionTypeEnum : int
    {
        Obshee = 1,
        PoluchenieDannih = 2,
        RaschetTailov = 3,
        SohranenieResultata = 4,
    }
}
 
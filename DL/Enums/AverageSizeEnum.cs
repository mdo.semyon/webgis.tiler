﻿namespace WebGIS.Tiler.DL.Enums
{
    public enum AverageSizeEnum : int
    {
        Small = 1,
        Medium = 2,
        Big = 3
    }
}

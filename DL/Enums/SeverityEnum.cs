﻿namespace WebGIS.Tiler.DL.Enums
{
    public enum SeverityEnum : int
    {
        Common = 1,
        Warning = 2,
        Error = 3
    }
}
 
﻿using System;
using Microsoft.SqlServer.Types;
using WebGIS.Core.DatabaseCore;
using System.Data;

namespace WebGIS.Tiler
{
    public class ErrorInfo
    {
        
        public int ObjectTypeID { get; set; }
        public int ErrorTypeID { get; set; }
        public int TileX { get; set; }
        public int TileY { get; set; }
        public int Zoom { get; set; }

        public static ErrorInfo FromReader(DataTableReader reader)
        {
            var result = new ErrorInfo
            {
                ObjectTypeID = reader.GetValue<int>("ObjectTypeID"),
                ErrorTypeID = reader.GetValue<int>("ErrorTypeID"),
                TileX = reader.GetValue<int>("TileX"),
                TileY = reader.GetValue<int>("TileY"),
                Zoom = reader.GetValue<int>("Zoom"),
            };

            return result;
        }

    }
}
 
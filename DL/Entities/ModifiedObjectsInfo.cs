﻿using System;
using Microsoft.SqlServer.Types;
using WebGIS.Core.DatabaseCore;
using System.Data;
using System.Data.SqlTypes;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using WebGIS.TileRendering;
using SharpVectors.Renderers.Wpf;
using SharpVectors.Converters;

namespace WebGIS.Tiler
{
    public class ColorStateInfo
    {
        public PointColorStateInfo PointColorState { get; set; }
        public PointColorStateInfo PointClusterColorState { get; set; }
        public Dictionary<int, PolylineColorStateInfo> PolylineColorStateDict { get; set; }
        public Dictionary<int, PolygonColorStateInfo> PolygonColorStateDict { get; set; }

        public ColorStateInfo()
        {
            PolylineColorStateDict = new Dictionary<int, PolylineColorStateInfo>();
            PolygonColorStateDict = new Dictionary<int, PolygonColorStateInfo>();
        }
    }

    public class PointColorStateInfo
    {
        public static Dictionary<int, double> Scales = new Dictionary<int, double>();
        public static Dictionary<int, double> ClusterScales = new Dictionary<int, double>();
        public static Dictionary<int, byte[]> PngImageStates = new Dictionary<int, byte[]>();
        public static Dictionary<int, int> LabelPositionStates = new Dictionary<int, int>();

        public byte[] SvgData { get; set; }
        public byte[] PngData { get; set; }
        public int StateID { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int LabelPosition { get; set; }

        public static PointColorStateInfo FromReader(DataTableReader reader)
        {
            var result = new PointColorStateInfo
            {
                StateID = reader.GetValue<int>("StateID"),
                Width = reader.GetValue<int>("Width"),
                Height = reader.GetValue<int>("Height"),
                LabelPosition = reader.GetValue<int>("LabelPositionID"),
            };

            if (reader["SvgData"] != null)
            {
                result.SvgData = (byte[])reader["SvgData"];
                result.PngData = PointColorStateInfo.convertSVGToPNG(result.SvgData);
            }
            else if (reader["PngData"] != null)
            {
                result.PngData = (byte[])reader["PngData"];
            }

            return result;
        }

        #region private
        static byte[] convertSVGToPNG(byte[] svgImage, double scale = 1.0)
        {
            if (svgImage == null)
                return null;

            try
            {
                WpfDrawingSettings settings = new WpfDrawingSettings();
                settings.IncludeRuntime = true;
                settings.TextAsGeometry = true;

                // 3. Create a file converter
                ImageSvgConverter converter = new ImageSvgConverter(settings);
                // 4. Perform the conversion to image  
                converter.EncoderType = ImageEncoderType.PngBitmap;


                byte[] image = null;
                using (var sourceImage = new MemoryStream(svgImage))
                {
                    using (MemoryStream targetImage = new MemoryStream())
                    {
                        if (converter.Convert(sourceImage, targetImage, scale))
                        {
                            image = targetImage.GetBuffer();
                        }
                    }
                }

                return image;
            }
            catch { }

            return null;
        }
        #endregion
    }
    public class PolylineColorStateInfo
    {
        public int StateID { get; set; }
        public string FillARGB { get; set; }
        public string StrokeARGB { get; set; }
        public string CenterARGB { get; set; }
        public int FillWidth { get; set; }
        public int StrokeWidth { get; set; }
        public int CenterWidth { get; set; }

        public static PolylineColorStateInfo FromReader(DataTableReader reader)
        {
            var result = new PolylineColorStateInfo
            {
                StateID = reader.GetValue<int>("StateID"),
                FillARGB = reader.GetValue<string>("FillARGB"),
                StrokeARGB = reader.GetValue<string>("StrokeARGB"),
                CenterARGB = reader.GetValue<string>("CenterARGB"),
                FillWidth = reader.GetValue<int>("FillWidth"),
                StrokeWidth = reader.GetValue<int>("StrokeWidth"),
                CenterWidth = reader.GetValue<int>("CenterWidth"),
            };

            return result;
        }
    }
    public class PolygonColorStateInfo
    {
        public int StateID { get; set; }
        public string FillARGB { get; set; }
        public string StrokeARGB { get; set; }
        public int StrokeWidth { get; set; }

        public static PolygonColorStateInfo FromReader(DataTableReader reader)
        {
            var result = new PolygonColorStateInfo
            {
                StateID = reader.GetValue<int>("StateID"),
                FillARGB = reader.GetValue<string>("FillARGB"),
                StrokeARGB = reader.GetValue<string>("StrokeARGB"),
                StrokeWidth = reader.GetValue<int>("StrokeWidth"),
            };

            return result;
        }
    }

    public class PointInfo
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int StateID { get; set; }

        public static PointInfo FromReader(DataTableReader reader)
        {
            var result = new PointInfo
            {
                Longitude = reader.GetValue<double>("Longitude"),
                Latitude = reader.GetValue<double>("Latitude"),
                StateID = reader.GetValue<int>("StateID")
            };

            return result;
        }
    }
    public class PointClusterInfo
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }

        public static PointClusterInfo FromReader(DataTableReader reader)
        {
            var result = new PointClusterInfo
            {
                Longitude = reader.GetValue<double>("Longitude"),
                Latitude = reader.GetValue<double>("Latitude"),
            };

            return result;
        }
    }
    public class PolylineInfo
    {
        public int StateID { get; set; }
        public SqlGeometry GeoData { get; set; }

        public static PolylineInfo FromReader(DataTableReader reader)
        {
            var result = new PolylineInfo
            {
                StateID = reader.GetValue<int>("StateID"),
                GeoData = reader.GetValue<SqlGeometry>("GeoData"),
            };

            return result;
        }
    }
    public class PolygonInfo
    {
        public int StateID { get; set; }
        public SqlGeometry GeoData { get; set; }

        public static PolygonInfo FromReader(DataTableReader reader)
        {
            var result = new PolygonInfo
            {
                StateID = reader.GetValue<int>("StateID"),
                GeoData = reader.GetValue<SqlGeometry>("GeoData")
            };
            return result;
        }
    }
}

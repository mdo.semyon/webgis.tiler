﻿using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebGIS.Tiler
{
    public class QuadkeyInfo
    {
        public string Quadkey { get; set; }
        public int Mode { get; set; }
    }
}
 
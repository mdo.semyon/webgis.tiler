﻿using System;
using Microsoft.SqlServer.Types;
using WebGIS.Core.DatabaseCore;
using System.Data;

namespace WebGIS.Tiler
{
    public class ModifiedTypesInfo
    {
        public int GeometryTypeID { get; set; }
        public int ObjectTypeID { get; set; }
        public int GeoSectionObjectID { get; set; }
        public int LinkedRasterGeoSectionObjectID { get; set; }
        public string ObjectTypeSysName { get; set; }

        //public int? QueueID { get; set; }
        //public DateTime LastUpdateTime { get; set; }
        public int ZoomBitMask { get; set; }
        public int ClusterBitMask { get; set; }
        public double AvgSize { get; set; }
        public DateTime? LastGenerationTimeUTC { get; set; }
        public int ErrorCount { get; set; }
        public bool? FailedQuadkeysPresent { get; set; }
        public bool AllQuadkeysWasProceededOnLastRun { get; set; }

        public static ModifiedTypesInfo FromReader(DataTableReader reader)
        {
            var result = new ModifiedTypesInfo
            {
                GeometryTypeID = reader.GetValue<int>("GeometryTypeID"),
                ObjectTypeID = reader.GetValue<int>("ObjectTypeID"),
                GeoSectionObjectID = reader.GetValue<int>("GeoSectionObjectID"),
                LinkedRasterGeoSectionObjectID = reader.GetValue<int>("LinkedRasterGeoSectionObjectID"),
                //GeoSectionSysName = reader.GetValue<string>("GeoSectionSysName"),
                ObjectTypeSysName = reader.GetValue<string>("ObjectTypeSysName"),
                //QueueID = reader.GetValue<int?>("QueueID"),
                //LastUpdateTime = reader.GetValue<DateTime>("LastUpdateTime"),
                ZoomBitMask = reader.GetValue<int>("ZoomBitMask"),
                ClusterBitMask = reader.GetValue<int>("ClusterBitMask"),
                AvgSize = reader.GetValue<double>("AvgSize"),
                LastGenerationTimeUTC = reader.GetValue<DateTime?>("LastGenerationTimeUTC"),
                ErrorCount = reader.GetValue<int>("ErrorCount"),
                FailedQuadkeysPresent = reader.GetValue<bool?>("FailedQuadkeysPresent"),
                AllQuadkeysWasProceededOnLastRun = reader.GetValue<bool>("AllQuadkeysWasProceededOnLastRun")
            };

            return result;
        }

    }
}
 
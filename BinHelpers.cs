﻿using System;

namespace WebGIS.Tiler
{
    public class BinHelpers
    {
        public static byte setBit(byte state, byte bit)
        {
            byte b = (byte)(state & bit);
            if (b == 0)
                state = (byte)(state | bit);

            return state;
        }
        public static byte setBits(byte state, byte[] bits)
        {
            byte b;
            for (int i = 0; i < bits.Length; i++)
            {
                b = (byte)(state & bits[i]);
                if (b == 0)
                    state = (byte)(state | bits[i]);
            }
            return state;
        }
        public static byte clearBit(byte state, byte bit)
        {
            state = (byte)(state & ~bit);
            return state;
        }
        public static byte clearBits(byte state, byte[] bits)
        {
            for (int i = 0; i < bits.Length; i++)
                state = (byte)(state & ~bits[i]);

            return state;
        }
        public static bool checkBit(byte state, byte bit)
        {
            state = (byte)(state & bit);
            return state != 0;
        }
        public static bool checkBits(byte state, byte[] bits)
        {
            byte b;
            for (int i = 0; i < bits.Length; i++)
            {
                b = (byte)(state & bits[i]);
                if (b == 0)
                    return false;
            }
            return true;
        }
    }
}

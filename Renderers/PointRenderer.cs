﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebGIS.Tiler.DL.Enums;
using WebGIS.Tiler.Logging;
using WebGIS.Tiler.Repository;
using WebGIS.TileRendering;
using WebGIS.Core.DatabaseCore;
using WebGis.Tiler.Utils;
using System.Drawing.Imaging;
using WebGIS.TileRendering.Conversions;
using System.Diagnostics;
using Microsoft.SqlServer.Types;

namespace WebGIS.Tiler.Renderers
{
    public static class PointRenderer
    {
        public static void ThreadProcPoint_I(List<string> subQuadkeys, int zoom, bool isCluster)
        {
            ProcessThread pt = Process.GetCurrentProcess().Threads.OfType<ProcessThread>().Single(pt_ => pt_.Id == Renderer.GetCurrentThreadId());
            pt.ProcessorAffinity = (IntPtr)Renderer.AffinityMask;

            if (subQuadkeys.Count == 0)
            {
                Renderer.ActiveThreads--;

                if (Renderer.ActiveThreads < Renderer.MaxActiveThreads + 1)
                    Renderer.eventHandle.Set();

                return;
            }

            string quadkeys = subQuadkeys[0];
            for (int i = 1; i < subQuadkeys.Count; i++)
                quadkeys += "." + subQuadkeys[i];

            using (SqlConnection conn = new SqlConnection(Renderer.tilerConfig.WebGIS_IN))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("[tiler].[GetNeighbourObjectsByQuadkeys]", conn) { CommandTimeout = 720, CommandType = CommandType.StoredProcedure };
                    cmd.Parameters.Add(new SqlParameter("@objectTypeID", SqlDbType.Int) { Value = Renderer.ObjectTypeID });
                    cmd.Parameters.Add(new SqlParameter("@quadkeys", SqlDbType.NVarChar) { Value = quadkeys });
                    cmd.Parameters.Add(new SqlParameter("@isCluster", SqlDbType.Bit) { Value = isCluster });

                    object positionOffset = null;
                    int width = 0, height = 0;
                    if (isCluster)
                    {
                        if (PointColorStateInfo.ClusterScales.ContainsKey(zoom))
                        {
                            width = (int)(Renderer.colorState.PointClusterColorState.Width * PointColorStateInfo.ClusterScales[zoom]);
                            height = (int)(Renderer.colorState.PointClusterColorState.Height * PointColorStateInfo.ClusterScales[zoom]);
                            positionOffset = _getPositionOffset(width,
                                                                height,
                                                                Renderer.colorState.PointClusterColorState.LabelPosition);                            
                        }
                        else if (Renderer.colorState.PointClusterColorState.PngData != null && Renderer.colorState.PointClusterColorState.PngData.Length > 0)
                        {
                            width = Renderer.colorState.PointClusterColorState.Width;
                            height = Renderer.colorState.PointClusterColorState.Height;
                            positionOffset = _getPositionOffset(width,
                                                                height,
                                                                Renderer.colorState.PointClusterColorState.LabelPosition);                            
                        }
                    }
                    else
                    {
                        if (PointColorStateInfo.Scales.ContainsKey(zoom))
                        {
                            width = (int)(Renderer.colorState.PointColorState.Width * PointColorStateInfo.Scales[zoom]);
                            height = (int)(Renderer.colorState.PointColorState.Height * PointColorStateInfo.Scales[zoom]);
                            positionOffset = _getPositionOffset(width,
                                                                height,
                                                                Renderer.colorState.PointColorState.LabelPosition);
                        }
                        else if (Renderer.colorState.PointColorState.PngData != null && Renderer.colorState.PointColorState.PngData.Length > 0)
                        {
                            width = Renderer.colorState.PointColorState.Width;
                            height = Renderer.colorState.PointColorState.Height;
                            positionOffset = _getPositionOffset(width,
                                                                height,
                                                                Renderer.colorState.PointClusterColorState.LabelPosition);
                        }
                    }
                    Point offset = (Point)positionOffset;
                    int maxOffcet = Math.Max(offset.X, offset.Y);

                    cmd.Parameters.Add(new SqlParameter("@maxOffcet", SqlDbType.Int) { Value = maxOffcet });

                    conn.Open();
                    var tables = cmd.GetDataSet().Tables;

                    int i = 0;
                    while (true)
                    {
                        if (tables[i].Rows.Count == 0)
                        {
                            deleteTile(subQuadkeys[i]);

                            i++;
                            if (i >= tables.Count)
                                break;
                            else
                                continue;
                        }

                        Renderer.semaphorHandle.Wait();
                        string quadkey = subQuadkeys[i];
                        DataTable table = tables[i].Clone();
                        foreach (DataRow r in tables[i].Rows)
                            table.Rows.Add(r.ItemArray);

                        bool _isCluster = isCluster;
                        System.Threading.Thread t = new System.Threading.Thread(() => ThreadProcPoint_II(table, quadkey, _isCluster));
                        t.Start();

                        i++;
                        if (i >= tables.Count)
                            break;
                    }
                }
                catch (Exception ex)
                {
                    TilerRepository.AddFailedQuadkey(Renderer.ObjectTypeID, Renderer.GeoSectionObjectID, quadkeys, zoom);
                    LogManager.LogCommon("Ошибка при вызове хранимой процедуры [tiler].[GetNeighbourObjectsByQuadkeys].", ex.Message, ActionTypeEnum.PoluchenieDannih, SeverityEnum.Error);
                }
            }

            Renderer.ActiveThreads--;

            if (Renderer.ActiveThreads < Renderer.MaxActiveThreads + 1)
                Renderer.eventHandle.Set();
        }
        public static void ThreadProcPoint_II(DataTable table, string subQuadkey, bool isCluster)
        {
            if (string.IsNullOrEmpty(subQuadkey))
                return;

            ProcessThread pt = Process.GetCurrentProcess().Threads.OfType<ProcessThread>().Single(pt_ => pt_.Id == Renderer.GetCurrentThreadId());
            pt.ProcessorAffinity = (IntPtr)Renderer.AffinityMask;

            Console.WriteLine(string.Format("zoom : {0}, quadkey : {1}", subQuadkey.Length, subQuadkey));

            int tileX, tileY, level;
            using (DataTableReader reader = table.CreateDataReader())
            {
                if (reader.HasRows)
                {
                    Helpers.QuadKeyToTileXY(subQuadkey, out tileX, out tileY, out level);

                    if (isCluster)
                    {
                        List<PointClusterInfo> geomInfoList = new List<PointClusterInfo>();

                        while (reader.Read())
                        {
                            var obj = PointClusterInfo.FromReader(reader);
                            geomInfoList.Add(new PointClusterInfo { Latitude = obj.Latitude, Longitude = obj.Longitude});
                        }

                        renderClusters(geomInfoList, tileX, tileY, level);
                    }
                    else
                    {
                        List<PointInfo> geomInfoList = new List<PointInfo>();

                        while (reader.Read())
                        {
                            var obj = PointInfo.FromReader(reader);
                            geomInfoList.Add(obj);
                        }

                        renderPoints(geomInfoList, tileX, tileY, level);
                    }
                }
            }
            Renderer.semaphorHandle.Release();
        }

        public static void renderPoints(List<PointInfo> geomInfoList, 
            int tileX, 
            int tileY, 
            int zoom)
        {
            byte[] result = null;

            Graphics graphics;
            Bitmap bitmap = new Bitmap(256, 256);
            using (Graphics xGraph = Graphics.FromImage(bitmap))
            {
                xGraph.FillRectangle(Brushes.Transparent, 0, 0, 256, 256);
                graphics = Graphics.FromImage(bitmap);
            }

            foreach (var gi in geomInfoList)
            {
                try
                {
                    
                    Bitmap iconImage = null;
                    byte[] imageData = new byte[0];
                    if (PointColorStateInfo.PngImageStates.ContainsKey(gi.StateID))
                    {
                        imageData = PointColorStateInfo.PngImageStates[gi.StateID];
                    }

                    int labelPosition = 0;
                    if (PointColorStateInfo.LabelPositionStates.ContainsKey(gi.StateID))
                    {
                        labelPosition = PointColorStateInfo.LabelPositionStates[gi.StateID];
                    }
                        
                    if (imageData != null && imageData.Length > 0)
                    {
                        using (MemoryStream ms = new MemoryStream(imageData))
                        {
                            iconImage = new Bitmap(ms, false);
                            double scale = 1.0;
                            if (PointColorStateInfo.Scales.ContainsKey(zoom))
                                scale = PointColorStateInfo.Scales[zoom];

                            if (scale != 1.0)
                            {
                                int width = (int)(iconImage.Width * scale);
                                int height = (int)(iconImage.Height * scale);
                                iconImage = iconImage.ResizeImage(width, height);
                            }
                        }
                    }

                    Icon2TileRendering.DrawIconOnTile(graphics, 
                        iconImage, 
                        zoom, 
                        gi.Longitude, 
                        gi.Latitude,
                        labelPosition, 
                        tileX, 
                        tileY);
                }
                catch (Exception ex)
                {
                    TilerRepository.AddFailedQuadkey(Renderer.ObjectTypeID, Renderer.GeoSectionObjectID, zoom, tileX, tileY);
                    LogManager.LogError("Во время расчёта тайлов произошла обшибка.", ex.Message, zoom, tileX, tileY, ActionTypeEnum.RaschetTailov, SeverityEnum.Error);
                }
            }
            result = bitmap.ToByteArray(ImageFormat.Png);
            if (result != null)
            {
                saveTiles(result, tileX, tileY, zoom);
            }

            if (graphics != null) graphics.Dispose();
            if (bitmap != null) bitmap.Dispose();
        }

        public static void renderClusters(List<PointClusterInfo> geomInfoList,
            int tileX,
            int tileY,
            int zoom)
        {
            byte[] result = null;

            Graphics graphics;
            Bitmap bitmap = new Bitmap(256, 256);
            using (Graphics xGraph = Graphics.FromImage(bitmap))
            {
                xGraph.FillRectangle(Brushes.Transparent, 0, 0, 256, 256);
                graphics = Graphics.FromImage(bitmap);
            }

            foreach (var gi in geomInfoList)
            {
                try
                {
                    Bitmap iconImage = null;
                    byte[] imageData = Renderer.colorState.PointClusterColorState.PngData;

                    if (imageData != null && imageData.Length > 0)
                    {
                        using (MemoryStream ms = new MemoryStream(imageData))
                        {
                            iconImage = new Bitmap(ms, false);
                            double scale = 1.0;
                            if (PointColorStateInfo.ClusterScales.ContainsKey(zoom))
                                scale = PointColorStateInfo.ClusterScales[zoom];

                            if (scale != 1.0)
                            {
                                int width = (int)(iconImage.Width * scale);
                                int height = (int)(iconImage.Height * scale);
                                iconImage = iconImage.ResizeImage(width, height);
                            }
                        }
                    }

                    Icon2TileRendering.DrawIconOnTile(graphics,
                        iconImage,
                        zoom,
                        gi.Longitude,
                        gi.Latitude,
                        Renderer.colorState.PointClusterColorState.LabelPosition,
                        tileX,
                        tileY);
                }
                catch (Exception ex)
                {
                    TilerRepository.AddFailedQuadkey(Renderer.ObjectTypeID, Renderer.GeoSectionObjectID, zoom, tileX, tileY);
                    LogManager.LogError("Во время расчёта тайлов произошла обшибка.", ex.Message, zoom, tileX, tileY, ActionTypeEnum.RaschetTailov, SeverityEnum.Error);
                }
            }
            result = bitmap.ToByteArray(ImageFormat.Png);
            if (result != null)
            {
                saveTiles(result, tileX, tileY, zoom);
            }

            if (graphics != null) graphics.Dispose();
            if (bitmap != null) bitmap.Dispose();
        }

        static void saveTiles(byte[] data, int tileX, int tileY, int zoom)
        {
            Bitmap compositeImage = new Bitmap(256, 256);
            Graphics compositeGraphics = Graphics.FromImage(compositeImage);
            compositeGraphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;

            PngConvertor convertor = null;
            Bitmap output = null;

            if (Renderer.tilerConfig.ConvertTo8Bit)
                convertor = new PngConvertor();

            try
            {
                using (MemoryStream ms = new MemoryStream(data))
                using (var img = Image.FromStream(ms))
                {
                    compositeGraphics.DrawImage(img, new Rectangle(0, 0, 256, 256), 0, 0, 256, 256, GraphicsUnit.Pixel);
                }

                if (Renderer.tilerConfig.ConvertTo8Bit)
                    output = convertor.ConvertTo8bppFormat(compositeImage);

                var pathX = Path.Combine(Renderer.tilerConfig.OutputFolder, Renderer.ObjectTypeSysName, string.Format("gso{0}", Renderer.LinkedRasterGeoSectionObjectID), zoom.ToString(), tileX.ToString());
                var pathY = Path.Combine(Renderer.tilerConfig.OutputFolder, Renderer.ObjectTypeSysName, string.Format("gso{0}", Renderer.LinkedRasterGeoSectionObjectID), zoom.ToString(), tileX.ToString(), tileY.ToString() + ".png");

                if (!Directory.Exists(pathX))
                    Directory.CreateDirectory(pathX);

                if (File.Exists(pathY))
                    File.Delete(pathY);

                byte[] im_data = Renderer.tilerConfig.ConvertTo8Bit ? output.ToByteArray(ImageFormat.Png) : compositeImage.ToByteArray(ImageFormat.Png);
                if (im_data.Length > Renderer.tilerConfig.EmptyTileSize)
                {
                    if (!File.Exists(pathY))
                    {
                        using (var fs = new FileStream(pathY, FileMode.CreateNew, FileAccess.Write))
                        {
                            fs.Write(im_data, 0, im_data.Length);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                TilerRepository.AddFailedQuadkey(Renderer.ObjectTypeID, Renderer.GeoSectionObjectID, zoom, tileX, tileY);
                LogManager.LogError("- твой файл был так велик и, должно быть, весьма полезен, но его больше нет.", ex.Message, zoom, tileX, tileY, ActionTypeEnum.SohranenieResultata, SeverityEnum.Error);
            }
            finally
            {
                compositeGraphics.Dispose();
                compositeImage.Dispose();
                compositeGraphics = null;
                compositeImage = null;
                if (output != null) output.Dispose();
                if (convertor != null) convertor.Dispose();
            }
        }
        static void deleteTile(string quadkey)
        {
            int tileX, tileY, zoom;
            Helpers.QuadKeyToTileXY(quadkey, out tileX, out tileY, out zoom);

            var pathY = Path.Combine(Renderer.tilerConfig.OutputFolder, Renderer.ObjectTypeSysName, string.Format("gso{0}", Renderer.LinkedRasterGeoSectionObjectID), zoom.ToString(), tileX.ToString(), tileY.ToString() + ".png");

            if (File.Exists(pathY))
                File.Delete(pathY);
        }

        /// <summary>
        /// Расчитывает смещение картинки относительно центра точечного объекта
        /// </summary>
        /// <param name="width">Ширина картинки</param>
        /// <param name="height">Высота картинки</param>
        /// <param name="stateID">Идентификатор состояния</param>
        /// <returns></returns>
        static Point _getPositionOffset(int width, int height, int labelPositionID)
        {
            Point fs = new Point();

            switch (labelPositionID)
            {
                case 0://Center
                    fs.X = (int)(width / 2);
                    fs.Y = (int)(height / 2);
                    break;
                case 1://Left
                    fs.X = width;
                    fs.Y = (int)(height / 2);
                    break;
                case 2://Right
                    fs.X = 0;
                    fs.Y = (int)(height / 2);
                    break;
                case 3://Top
                    fs.X = (int)(width / 2);
                    fs.Y = height;
                    break;
                case 4://Bottom
                    fs.X = (int)(width / 2);
                    fs.Y = 0;
                    break;
            }

            return fs;
        }

    }
}

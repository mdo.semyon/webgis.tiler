﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebGis.Tiler.Utils;
using WebGIS.Tiler.DL.Enums;

namespace WebGIS.Tiler.Renderers
{ 
    public class Renderer
    {
        [DllImport("kernel32.dll")]
        public static extern IntPtr GetCurrentThread();
        [DllImport("kernel32.dll")]
        public static extern IntPtr SetThreadAffinityMask(IntPtr hThread, IntPtr dwThreadAffinityMask);
        [DllImport("kernel32.dll")]
        public static extern uint GetCurrentThreadId();

        public static int AffinityMask = 0;

        public static int ActiveThreads = 0;
        public static int MaxActiveThreads = 40;

        public static int ObjectTypeID { get; set; }
        public static int GeoSectionObjectID { get; set; }
        public static int LinkedRasterGeoSectionObjectID { get; set; }
        public static string ObjectTypeSysName { get; set; }

        public static GeometryTypeEnum GeometryType { get; set; }
        public static ColorStateInfo colorState = null;
        public static TilerConfig tilerConfig = null;

        public static System.Threading.ManualResetEvent eventHandle = new System.Threading.ManualResetEvent(false);
        public static System.Threading.ManualResetEvent updateObjectTypeRecordEventHandle = new System.Threading.ManualResetEvent(false);
        public static System.Threading.SemaphoreSlim semaphorHandle = null;

        //public static ThreadPool Pool = new ThreadPool.cle
    }
}

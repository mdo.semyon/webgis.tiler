﻿using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebGIS.Tiler.DL.Enums;
using WebGIS.Tiler.Logging;
using WebGIS.Tiler.Repository;
using WebGIS.TileRendering;
using WebGIS.TileRendering.Conversions;
using WebGIS.Core.DatabaseCore;
using WebGis.Tiler.Utils;
using System.Diagnostics;

namespace WebGIS.Tiler.Renderers
{

    public static class BigMultipolygonRenderer
    {
        public static void ThreadProcMultipolygon(string quadkey, int zoom)
        {
            if (string.IsNullOrEmpty(quadkey))
                return;

            ProcessThread pt = Process.GetCurrentProcess().Threads.OfType<ProcessThread>().Single(pt_ => pt_.Id == Renderer.GetCurrentThreadId());
            pt.ProcessorAffinity = (IntPtr)Renderer.AffinityMask;

            Console.WriteLine(string.Format("zoom : {0}, quadkey : {1}", zoom, quadkey));

            if (string.IsNullOrEmpty(quadkey))
            {
                Renderer.ActiveThreads--;

                if (Renderer.ActiveThreads < Renderer.MaxActiveThreads + 1)
                    Renderer.eventHandle.Set();

                return;
            }

            int tileX, tileY, level;
            Helpers.QuadKeyToTileXY(quadkey, out tileX, out tileY, out level);

            using (SqlConnection conn = new SqlConnection(Renderer.tilerConfig.WebGIS_IN))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("[tiler].[GetNeighbourObjectsByQuadkeys]", conn) { CommandTimeout = 720, CommandType = CommandType.StoredProcedure };
                    cmd.Parameters.Add(new SqlParameter("@objectTypeID", SqlDbType.Int) { Value = Renderer.ObjectTypeID });
                    cmd.Parameters.Add(new SqlParameter("@quadkeys", SqlDbType.NVarChar) { Value = quadkey });

                    conn.Open();
                    var tables = cmd.GetDataSet().Tables;

                    if (tables.Count > 0)
                    {
                        using (DataTableReader reader = tables[0].CreateDataReader())
                        {
                            if (reader.HasRows)
                            {
                                List<PolygonInfo> geomInfoList = new List<PolygonInfo>();

                                while (reader.Read())
                                {
                                    var obj = PolygonInfo.FromReader(reader);
                                    if (obj.GeoData.GetGeometryType() == OpenGisGeometryType.GeometryCollection)
                                    {
                                        var geomNum = obj.GeoData.STNumGeometries();
                                        if (geomNum.Value > 0)
                                        {
                                            geomInfoList.Add(obj);
                                        }
                                    }
                                    else
                                    {
                                        geomInfoList.Add(obj);
                                    }
                                }

                                if (geomInfoList.Count > 0)
                                {

                                    double lon = Helpers.getLongitudeFromTilePos(tileX, level);
                                    double lat = Helpers.getLatitudeFromTilePos(tileY, level);

                                    //0.
                                    tileX = Helpers.getXTilePos(lon, zoom);
                                    tileY = Helpers.getYTilePos(lat, zoom);

                                    List<PolygonInfo> geomsInPixels = new List<PolygonInfo>();
                                    foreach (var g in geomInfoList)
                                    {
                                        var newg = ShapeToTileRendering.ConvertToPixels(g.GeoData, zoom);
                                        geomsInPixels.Add(new PolygonInfo { GeoData = newg, StateID = g.StateID });
                                    }

                                    recourciveCreateThreadsTree(geomsInPixels,
                                        tileX,
                                        tileY,
                                        zoom,
                                        level,
                                        0);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    TilerRepository.AddFailedQuadkey(Renderer.ObjectTypeID, Renderer.GeoSectionObjectID, quadkey, zoom);
                    LogManager.LogCommon("Ошибка при вызове хранимой процедуры [tiler].[GetNeighbourObjectsByQuadkeys].", ex.Message, ActionTypeEnum.PoluchenieDannih, SeverityEnum.Error);
                }
            }

            Renderer.ActiveThreads--;

            if (Renderer.ActiveThreads < Renderer.MaxActiveThreads + 1)
                Renderer.eventHandle.Set();
        }
        public static void recourciveCreateThreadsTree(List<PolygonInfo> geomInfoList, int tileX, int tileY, int zoom, int level, int mesh_level)
        {
            if (zoom - level > mesh_level)
            {
                //0.
                recourciveCreateThreadsTree(geomInfoList,
                    tileX,
                    tileY,
                    zoom,
                    level + 1,
                    mesh_level);

                //1.
                recourciveCreateThreadsTree(geomInfoList,
                    tileX + (int)Math.Pow(2, zoom - level - 1),
                    tileY,
                    zoom,
                    level + 1,
                    mesh_level);
                //2.
                recourciveCreateThreadsTree(geomInfoList,
                    tileX,
                    tileY + (int)Math.Pow(2, zoom - level - 1),
                    zoom,
                    level + 1,
                    mesh_level);
                //3.
                recourciveCreateThreadsTree(geomInfoList,
                    tileX + (int)Math.Pow(2, zoom - level - 1),
                    tileY + (int)Math.Pow(2, zoom - level - 1),
                    zoom,
                    level + 1,
                    mesh_level);
            }
            else
            {
                Renderer.semaphorHandle.Wait();
                System.Threading.Thread t = new System.Threading.Thread(() => renderMultipolygon(geomInfoList,
                    tileX,
                    tileX + (int)Math.Pow(2, /*zoom - */mesh_level),
                    tileY,
                    tileY + (int)Math.Pow(2, /*zoom - */mesh_level),
                    zoom,
                    zoom - level));
                t.Start();
            }
        }
        public static void renderMultipolygon(List<PolygonInfo> geomInfoList,
            int leftX,
            int rightX,
            int topY,
            int bottomY,
            int zoom,
            int mesh_level)
        {
            byte[] result = null;
            int mesh_size = (int)Math.Pow(2, mesh_level);

            Graphics graphics;
            Bitmap bitmap = new Bitmap(mesh_size * 256, mesh_size * 256);
            using (Graphics xGraph = Graphics.FromImage(bitmap))
            {
                xGraph.FillRectangle(Brushes.Transparent, 0, 0, mesh_size * 256, mesh_size * 256);
                graphics = Graphics.FromImage(bitmap);
            }

            Color fillARGB, strokeARGB;
            int strokeWidth;
            foreach (var gi in geomInfoList)
            {
                try
                {
                    if (Renderer.colorState.PolygonColorStateDict.ContainsKey(gi.StateID))
                    {
                        var cs = Renderer.colorState.PolygonColorStateDict[gi.StateID];
                        fillARGB = string.IsNullOrEmpty(cs.FillARGB) ? Color.Transparent : Color.FromArgb(Int32.Parse(cs.FillARGB, NumberStyles.HexNumber));
                        strokeARGB = string.IsNullOrEmpty(cs.StrokeARGB) ? Color.Transparent : Color.FromArgb(Int32.Parse(cs.StrokeARGB, NumberStyles.HexNumber));
                        strokeWidth = cs.StrokeWidth;

                        if (strokeARGB == Color.Transparent)
                        {
                            var cutted = ShapeToTileRendering.Cut(gi.GeoData, leftX, rightX, topY, bottomY);
                            ShapeToTileRendering.ShiftAndDraw(graphics, cutted, fillARGB, leftX, topY);
                        }
                        else
                        {
                            var cutted = ShapeToTileRendering.CutWithStroke(gi.GeoData, leftX, rightX, topY, bottomY);
                            ShapeToTileRendering.ShiftAndDraw(graphics, cutted, fillARGB, strokeARGB, Color.Transparent, 0, strokeWidth, 0, leftX, topY);
                        }
                    }
                }
                catch (Exception ex)
                {
                    TilerRepository.AddFailedQuadkey(Renderer.ObjectTypeID, Renderer.GeoSectionObjectID, zoom, leftX, topY);
                    LogManager.LogError("Во время расчёта тайлов произошла обшибка.", ex.Message, zoom, leftX, topY, ActionTypeEnum.RaschetTailov, SeverityEnum.Error);
                }
            }
            result = bitmap.ToByteArray(ImageFormat.Png);
            if (result != null)
            {
                saveTiles(Renderer.ObjectTypeID, result, leftX, rightX, topY, bottomY, zoom, mesh_level);
            }

            if (graphics != null) graphics.Dispose();
            if (bitmap != null) bitmap.Dispose();

            Renderer.semaphorHandle.Release();
        }
        static void saveTiles(int objectTypeID, byte[] data, int leftX, int rightX, int topY, int bottomY, int zoom, int mesh_level)
        {
            int mesh_size = (int)Math.Pow(2, mesh_level);

            Bitmap compositeImage = new Bitmap(mesh_size * 256, mesh_size * 256);
            Graphics compositeGraphics = Graphics.FromImage(compositeImage);
            compositeGraphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;

            PngConvertor convertor = null;
            Bitmap output = null;

            if (Renderer.tilerConfig.ConvertTo8Bit)
                convertor = new PngConvertor();

            try
            {
                using (MemoryStream ms = new MemoryStream(data))
                using (var img = Image.FromStream(ms))
                {
                    compositeGraphics.DrawImage(img, new Rectangle(0, 0, mesh_size * 256, mesh_size * 256), 0, 0, mesh_size * 256, mesh_size * 256, GraphicsUnit.Pixel);
                }

                if (Renderer.tilerConfig.ConvertTo8Bit)
                    output = convertor.ConvertTo8bppFormat(compositeImage);

                var pathX = Path.Combine(Renderer.tilerConfig.OutputFolder, Renderer.ObjectTypeSysName, string.Format("gso{0}", Renderer.LinkedRasterGeoSectionObjectID), zoom.ToString(), leftX.ToString());
                var pathY = Path.Combine(Renderer.tilerConfig.OutputFolder, Renderer.ObjectTypeSysName, string.Format("gso{0}", Renderer.LinkedRasterGeoSectionObjectID), zoom.ToString(), leftX.ToString(), topY.ToString() + ".png");

                if (!Directory.Exists(pathX))
                    Directory.CreateDirectory(pathX);

                if (File.Exists(pathY))
                    File.Delete(pathY);

                byte[] im_data = Renderer.tilerConfig.ConvertTo8Bit ? output.ToByteArray(ImageFormat.Png) : compositeImage.ToByteArray(ImageFormat.Png);
                if (im_data.Length > Renderer.tilerConfig.EmptyTileSize)
                {
                    if (!File.Exists(pathY))
                    {
                        using (var fs = new FileStream(pathY, FileMode.CreateNew, FileAccess.Write))
                        {
                            fs.Write(im_data, 0, im_data.Length);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                TilerRepository.AddFailedQuadkey(Renderer.ObjectTypeID, Renderer.GeoSectionObjectID, zoom, leftX, topY);
                LogManager.LogError("Во время сохранения тайлов произошла обшибка.", ex.Message, zoom, leftX, topY, ActionTypeEnum.SohranenieResultata, SeverityEnum.Error);
            }
            finally
            {
                compositeGraphics.Dispose();
                compositeImage.Dispose();
                compositeGraphics = null;
                compositeImage = null;
                if (output != null) output.Dispose();
                if (convertor != null) convertor.Dispose();

            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Collections;
using System.Linq;

namespace WebGis.Tiler.Utils
{
    public class PngConvertor
    {
        #region classes
        class ColorsDistribution
        {
            public Color Clr { get; set; }
            public int Count { get; set; }

            public ColorsDistribution() { }
        }
        #endregion

        #region data
        Hashtable m_knownColors = new Hashtable((int)Math.Pow(2, 20), 1.0f);
        Dictionary<int, ColorsDistribution> _colors = new Dictionary<int, ColorsDistribution>();
        #endregion

        #region public
        public PngConvertor() { }
        public void Dispose()
        {
            m_knownColors.Clear();
            _colors.Clear();

            //m_knownColors = null;
            //_colors = null;
        }

        public Bitmap ConvertTo8bppFormat(Bitmap bmpSource)
        {
            int imageWidth = bmpSource.Width;
            int imageHeight = bmpSource.Height;

            Bitmap bmpDest = null;
            BitmapData bmpDataDest = null;
            BitmapData bmpDataSource = null;

            try
            {

                // Create new image with 8BPP format
                bmpDest = new Bitmap(
                    imageWidth,
                    imageHeight,
                    PixelFormat.Format8bppIndexed
                    );

                // Lock bitmap in memory
                bmpDataDest = bmpDest.LockBits(
                    new Rectangle(0, 0, imageWidth, imageHeight),
                    ImageLockMode.ReadWrite,
                    bmpDest.PixelFormat
                    );

                bmpDataSource = bmpSource.LockBits(
                    new Rectangle(0, 0, imageWidth, imageHeight),
                    ImageLockMode.ReadOnly,
                    bmpSource.PixelFormat
                );

                int pixelSize = GetPixelInfoSize(bmpDataSource.PixelFormat);
                byte[] buffer = new byte[imageWidth * imageHeight * pixelSize];
                byte[] destBuffer = new byte[imageWidth * imageHeight];

                // Read all data to buffer
                ReadBmpData(bmpDataSource, buffer, pixelSize, imageWidth, imageHeight);

                //configure Palette
                setupPalette(ref bmpDest, buffer, destBuffer.Length, pixelSize);

                // Get color indexes
                MatchColors(buffer, destBuffer, pixelSize, bmpDest.Palette);

                // Copy all colors to destination bitmaps
                WriteBmpData(bmpDataDest, destBuffer, imageWidth, imageHeight);

                return bmpDest;
            }
            finally
            {
                if (bmpDest != null) bmpDest.UnlockBits(bmpDataDest);
                if (bmpSource != null) bmpSource.UnlockBits(bmpDataSource);
            }
        }
        #endregion

        #region private
        void setupPalette(ref Bitmap img, byte[] srcBuffer, int dstBufferLength, int pixelSize)
        {
            byte[] temp = new byte[pixelSize];
            ColorPalette pal = img.Palette;

            int mult_1 = 256,
                mult_2 = 256 * 256,
                mult_3 = 256 * 256 * 256,
                currentKey = 0;

            for (int i = 0; i < dstBufferLength; i++)
            {
                Array.Copy(srcBuffer, i * pixelSize, temp, 0, pixelSize);
                currentKey = temp[2] + temp[1] * mult_1 + temp[0] * mult_2 + temp[3] * mult_3;

                if (!_colors.ContainsKey(currentKey))
                {
                    var clrsDistr = new ColorsDistribution() { Clr = Color.FromArgb(temp[3], temp[2], temp[1], temp[0]), Count = 1 };
                    _colors.Add(currentKey, clrsDistr);
                }
                else
                {
                    var val = _colors[currentKey] as ColorsDistribution;
                    if (val != null)
                        val.Count++;
                }
            }

            var res = _colors.OrderByDescending(o => o.Value.Count).Select(o => o.Value).ToArray();
            for (int j = 0; j < 256; j++)
                pal.Entries[j] = (res.Length > j) ? res[j].Clr : Color.FromArgb(255, 255, 255, 255);

            img.Palette = pal;
        }
        void MatchColors(byte[] buffer, byte[] destBuffer, int pixelSize, ColorPalette pallete)
        {
            byte[] temp = new byte[pixelSize];
            int length = destBuffer.Length,
                palleteSize = pallete.Entries.Length,
                mult_1 = 256,
                mult_2 = 256 * 256,
                mult_3 = 256 * 256 * 256,
                currentKey = 0;

            for (int i = 0; i < length; i++)
            {
                Array.Copy(buffer, i * pixelSize, temp, 0, pixelSize);
                currentKey = temp[2] + temp[1] * mult_1 + temp[0] * mult_2 + temp[3] * mult_3;

                if (!m_knownColors.ContainsKey(currentKey))
                {
                    destBuffer[i] = GetSimilarColor(pallete, temp, palleteSize);
                    m_knownColors.Add(currentKey, destBuffer[i]);
                }
                else
                {
                    destBuffer[i] = (byte)m_knownColors[currentKey];
                }
            }
        }
        int GetPixelInfoSize(PixelFormat format)
        {
            switch (format)
            {
                case PixelFormat.Format24bppRgb:
                    return 3;
                case PixelFormat.Format32bppArgb:
                    return 4;
                default:
                    throw new ApplicationException("Only 24bit and 32bit colors are supported now");
            }
        }
        void ReadBmpData(BitmapData bmpDataSource, byte[] buffer, int pixelSize, int width, int height)
        {
            Int64 addrStart = bmpDataSource.Scan0.ToInt64();

            for (int i = 0; i < height; i++)
            {
                IntPtr realByteAddr = new IntPtr(addrStart +
                    System.Convert.ToInt64(i * bmpDataSource.Stride)
                    );

                Marshal.Copy(
                    realByteAddr,
                    buffer,
                    (int)(i * width * pixelSize),
                    (int)(width * pixelSize)
                );
            }
        }
        void WriteBmpData(BitmapData bmpDataDest, byte[] destBuffer, int imageWidth, int imageHeight)
        {
            Int64 addrStart = bmpDataDest.Scan0.ToInt64();

            for (int i = 0; i < imageHeight; i++)
            {
                IntPtr realByteAddr = new IntPtr(addrStart +
                    System.Convert.ToInt64(i * bmpDataDest.Stride)
                    );

                Marshal.Copy(
                    destBuffer,
                    i * imageWidth,
                    realByteAddr,
                    imageWidth);
            }
        }
        byte GetSimilarColor(ColorPalette palette, byte[] color, int palleteSize)
        {
            byte minDiff = byte.MaxValue;
            int index = -1;

            if (color.Length == 4)
            {
                for (int i = 0; i < palleteSize; i++)
                {
                    if (color[3] == palette.Entries[i].A &&
                        color[2] == palette.Entries[i].R &&
                        color[1] == palette.Entries[i].G &&
                        color[0] == palette.Entries[i].B)
                    {
                        index = i;
                        break;
                    }
                }

                if (index == -1)
                {
                    for (int i = 0; i < palleteSize; i++)
                    {
                        byte currentDiff = GetMaxDiff(color, palette.Entries[i]);
                        if (currentDiff < minDiff)
                        {
                            minDiff = currentDiff;
                            index = i;
                        }
                    }
                }
            }
            else
                throw new ApplicationException("Only 24bit (todo 32bit) colors supported now");

            return (index == -1) ? (byte)0 : (byte)index;
        }
        static byte GetMaxDiff(byte[] a, Color b)
        {
            byte bDiff = a[0] > b.B ? (byte)(a[0] - b.B) : (byte)(b.B - a[0]);

            byte gDiff = a[1] > b.G ? (byte)(a[1] - b.G) : (byte)(b.G - a[1]);

            byte rDiff = a[2] > b.R ? (byte)(a[2] - b.R) : (byte)(b.R - a[2]);

            byte max = bDiff > gDiff ? bDiff : gDiff;

            max = max > rDiff ? max : rDiff;

            return max;
        }
        #endregion

    }
}
 
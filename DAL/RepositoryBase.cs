﻿using System.Configuration;
using System.Diagnostics;
using WebGIS.Core.DatabaseCore;
using WebGIS.Tiler.Repository;

namespace WebGIS.Tiler
{
    public abstract class RepositoryBase : IRepository
    {
        private string _csWebGIS;
        protected string WebGIS_OUT
        {
            get
            {
                if (string.IsNullOrEmpty(_csWebGIS))
                {
                    var css = ConfigurationManager.ConnectionStrings["WebGIS_OUT"];

                    if (css == null || string.IsNullOrEmpty(css.ConnectionString))
                    {
                        Trace.TraceError("Не найдена ConnectionString для БД.");
                    }
                    else
                    {
                        _csWebGIS = css.ConnectionString;
                    }
                }
                return _csWebGIS;
            }
        }

        public IDBConnectType GetConnectType()
        {
            return new DBConnectionType(WebGIS_OUT);
        }
    }
}
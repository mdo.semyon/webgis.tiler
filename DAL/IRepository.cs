﻿using WebGIS.Core.DatabaseCore;

namespace WebGIS.Tiler.Repository
{
    public interface IRepository
    {
        IDBConnectType GetConnectType();
    }
}

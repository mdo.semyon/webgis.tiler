﻿using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using WebGIS.Core;
using WebGIS.Core.DatabaseCore;
using WebGIS.Tiler.DL.Enums;
using WebGIS.Tiler.Logging;
using WebGIS.Tiler.Renderers;
using WebGIS.TileRendering.Conversions;
 
namespace WebGIS.Tiler.Repository
{
    public class TilerRepository : RepositoryBase
    {
        public List<ModifiedTypesInfo> GetModifiedTypes(out int RunID)
        {
            var res = new List<ModifiedTypesInfo>();
            
            RunID = 0;

            try
            {
                var connectType = new DBConnectionType(Renderer.tilerConfig.WebGIS_IN);
                using (var dbContext = new DBContext(connectType))
                {
                    using (var cmd = dbContext.CreateCommand("[tiler].[GetModifiedTypes]"))
                    {
                        cmd.CommandTimeout = 720;

                        var tables = cmd.GetDataSet().Tables;

                        using (DataTableReader reader = tables[0].CreateDataReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    res.Add(ModifiedTypesInfo.FromReader(reader));
                                }
                            }
                        }

                        using (DataTableReader reader = tables[1].CreateDataReader())
                        {
                            if (reader.HasRows)
                            {
                                reader.Read();
                                RunID = (int)reader["LastRunID"];
                            }
                        }

                    }
                }
            }
            catch(Exception ex)
            {
                LogManager.LogCommon("Ошибка при вызове хранимой процедуры [tiler].[GetModifiedTypes].", ex.Message, ActionTypeEnum.PoluchenieDannih, SeverityEnum.Error);
                res = null;
            }
            return res;
        }

        public ColorStateInfo GetColorStates(int objectTypeID, GeometryTypeEnum geometryType)
        {
            ColorStateInfo res = new ColorStateInfo();
            try
            {
                var connectType = new DBConnectionType(Renderer.tilerConfig.WebGIS_IN);
                using (var dbContext = new DBContext(connectType))
                {
                    using (var cmd = dbContext.CreateCommand("[tiler].[GetObjectsColorState]"))
                    {
                        cmd.AddParameter("@objectTypeID", SqlDbType.Int, objectTypeID);
                        cmd.AddParameter("@geometryTypeID", SqlDbType.Int, geometryType);

                        var tables = cmd.GetDataSet().Tables;

                        switch (geometryType)
                        {
                            case GeometryTypeEnum.Point:
                                using (DataTableReader reader = tables[0].CreateDataReader())
                                {
                                    if (reader.HasRows)
                                    {
                                        while (reader.Read())
                                        {
                                            res.PointColorState = PointColorStateInfo.FromReader(reader);
                                            PointColorStateInfo.PngImageStates.Add(res.PointColorState.StateID, res.PointColorState.PngData);
                                            PointColorStateInfo.LabelPositionStates.Add(res.PointColorState.StateID, res.PointColorState.LabelPosition);
                                        }
                                    }
                                }

                                using (DataTableReader reader = tables[1].CreateDataReader())
                                {
                                    if (reader.HasRows)
                                    {
                                        while (reader.Read())
                                        {
                                            res.PointClusterColorState = PointColorStateInfo.FromReader(reader);
                                        }
                                    }
                                }

                                using (DataTableReader reader = tables[2].CreateDataReader())
                                {
                                    if (reader.HasRows)
                                    {
                                        while (reader.Read())
                                        {
                                            int zoom = reader.GetValue<int>("Zoom");
                                            double scale = reader.GetValue<double>("Scale");
                                            PointColorStateInfo.Scales.Add(zoom, scale);
                                        }
                                    }
                                }

                                using (DataTableReader reader = tables[3].CreateDataReader())
                                {
                                    if (reader.HasRows)
                                    {
                                        while (reader.Read())
                                        {
                                            int startZoom = reader.GetValue<int>("StartZoom");
                                            int endZoom = reader.GetValue<int>("EndZoom");
                                            double scale = reader.GetValue<double>("Scale");

                                            for (var z = startZoom; z <= endZoom; z++)
                                            {
                                                PointColorStateInfo.ClusterScales.Add(z, scale);
                                            }
                                        }
                                    }
                                }

                                break;
                            case GeometryTypeEnum.Polyline:
                                using (DataTableReader reader = tables[0].CreateDataReader())
                                {
                                    if (reader.HasRows)
                                    {
                                        while (reader.Read())
                                        {
                                            var cs = PolylineColorStateInfo.FromReader(reader);
                                            res.PolylineColorStateDict.Add(cs.StateID, cs);
                                        }
                                    }
                                }
                                break;
                            case GeometryTypeEnum.Sector:
                            case GeometryTypeEnum.Polygon:
                            case GeometryTypeEnum.Multipolygon:
                                using (DataTableReader reader = tables[0].CreateDataReader())
                                {
                                    if (reader.HasRows)
                                    {
                                        while (reader.Read())
                                        {
                                            var cs = PolygonColorStateInfo.FromReader(reader);
                                            res.PolygonColorStateDict.Add(cs.StateID, cs);
                                        }
                                    }
                                }
                                break;
                            default: break;
                        }
                     
                    }
                }
            }
            catch(Exception ex)
            {
                LogManager.LogCommon("Ошибка при вызове хранимой процедуры [tiler].[GetObjectsColorState].", ex.Message, ActionTypeEnum.PoluchenieDannih, SeverityEnum.Error);
                res = null;
            }
            return res;
        }

        #region failed quadkeys
        //public static int GetFailedQuadkeys(int objectTypeID, int geoSectionObjectID)
        //{
        //    int res;

        //    try
        //    {
        //        var connectType = new DBConnectionType(Renderer.tilerConfig.WebGIS_IN);
        //        using (var dbContext = new DBContext(connectType))
        //        {
        //            using (var cmd = dbContext.CreateCommand("[tiler].[GetFailedQuadkeys]"))
        //            {
        //                cmd.AddParameter("@objectTypeID", SqlDbType.Int, objectTypeID);
        //                cmd.AddParameter("@geoSectionObjectID", SqlDbType.Int, geoSectionObjectID);

        //                res = (Int32)cmd.ExecuteScalar();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LogManager.LogCommon("Ошибка при вызове хранимой процедуры [tiler].[GetFailedQuadkeys].", ex.Message, ActionTypeEnum.Obshee, SeverityEnum.Error);
        //        return 0;
        //    }
        //    return res;
        //}
        public static bool AddFailedQuadkey(int objectTypeID, int geoSectionObjectID, string quadkeys, int zoom)
        {
            bool res = true;

            try
            {
                var connectType = new DBConnectionType(Renderer.tilerConfig.WebGIS_OUT);
                using (var dbContext = new DBContext(connectType))
                {
                    using (var cmd = dbContext.CreateCommand("[tiler].[AddFailedQuadkey]"))
                    {
                        cmd.AddParameter("@objectTypeID", SqlDbType.Int, objectTypeID);
                        cmd.AddParameter("@geoSectionObjectID", SqlDbType.Int, geoSectionObjectID);
                        cmd.AddParameter("@quadkeys", SqlDbType.NVarChar, quadkeys);
                        cmd.AddParameter("@zoom", SqlDbType.Int, zoom);

                        res = cmd.ExecuteNonQuery() > 0;
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.LogCommon("Ошибка при вызове хранимой процедуры [tiler].[AddFailedQuadkey].", ex.Message, ActionTypeEnum.Obshee, SeverityEnum.Error);
                return false;
            }
            return true;
        }
        public static bool AddFailedQuadkey(int objectTypeID, int geoSectionObjectID, int zoom, int tileX, int tileY)
        {
            bool res = true;
            string quadkey = Helpers.TileXYToQuadKey(tileX, tileY, zoom);
            try
            {
                var connectType = new DBConnectionType(Renderer.tilerConfig.WebGIS_OUT);
                using (var dbContext = new DBContext(connectType))
                {
                    using (var cmd = dbContext.CreateCommand("[tiler].[AddFailedQuadkey]"))
                    {
                        cmd.AddParameter("@objectTypeID", SqlDbType.Int, objectTypeID);
                        cmd.AddParameter("@geoSectionObjectID", SqlDbType.Int, geoSectionObjectID);
                        cmd.AddParameter("@quadkeys", SqlDbType.NVarChar, quadkey);
                        cmd.AddParameter("@zoom", SqlDbType.Int, zoom);

                        res = cmd.ExecuteNonQuery() > 0;
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.LogCommon("Ошибка при вызове хранимой процедуры [tiler].[AddFailedQuadkey].", ex.Message, ActionTypeEnum.Obshee, SeverityEnum.Error);
                return false;
            }
            return true;
        }
        public static bool PrepareFailedQuadkeys(int objectTypeID, int geoSectionObjectID)
        {
            bool res = true;

            try
            {
                var connectType = new DBConnectionType(Renderer.tilerConfig.WebGIS_OUT);
                using (var dbContext = new DBContext(connectType))
                {
                    using (var cmd = dbContext.CreateCommand("[tiler].[PrepareFailedQuadkeys]"))
                    {
                        cmd.AddParameter("@objectTypeID", SqlDbType.Int, objectTypeID);
                        cmd.AddParameter("@geoSectionObjectID", SqlDbType.Int, geoSectionObjectID);
                        res = cmd.ExecuteNonQuery() > 0;
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.LogCommon("Ошибка при вызове хранимой процедуры [tiler].[PrepareFailedQuadkeys].", ex.Message, ActionTypeEnum.Obshee, SeverityEnum.Error);
                return false;
            }
            return true;
        }
        public static bool DeleteFixedQuadkeys(int objectTypeID, int geoSectionObjectID)
        {
            bool res = true;

            try
            {
                var connectType = new DBConnectionType(Renderer.tilerConfig.WebGIS_OUT);
                using (var dbContext = new DBContext(connectType))
                {
                    using (var cmd = dbContext.CreateCommand("[tiler].[DeleteFixedQuadkeys]"))
                    {
                        cmd.AddParameter("@objectTypeID", SqlDbType.Int, objectTypeID);
                        cmd.AddParameter("@geoSectionObjectID", SqlDbType.Int, geoSectionObjectID);
                        res = cmd.ExecuteNonQuery() > 0;
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.LogCommon("Ошибка при вызове хранимой процедуры [tiler].[DeleteFixedQuadkeys].", ex.Message, ActionTypeEnum.Obshee, SeverityEnum.Error);
                return false;
            }
            return true;
        }
        #endregion

        #region Activity monitor
        public bool IncrementErrorCounts(int objectTypeID, int geoSectionObjectID)
        {
            bool res = true;

            try
            {
                var connectType = new DBConnectionType(Renderer.tilerConfig.WebGIS_OUT);
                using (var dbContext = new DBContext(connectType))
                {
                    using (var cmd = dbContext.CreateCommand("[tiler].[IncrementErrorCounts]"))
                    {
                        cmd.AddParameter("@objectTypeID", SqlDbType.Int, objectTypeID);
                        cmd.AddParameter("@geoSectionObjectID", SqlDbType.Int, geoSectionObjectID);
                        res = cmd.ExecuteNonQuery() > 0;
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.LogCommon("Ошибка при вызове хранимой процедуры [tiler].[IncrementErrorCounts].", ex.Message, ActionTypeEnum.Obshee, SeverityEnum.Error);
                return false;
            }
            return true;
        }

        public bool UpdateObjectTypeRecordOnSuccess(DateTime updateDateUTC, DateTime updateDateLocal)
        {
            bool res = false;

            try
            {
                var connectType = new DBConnectionType(Renderer.tilerConfig.WebGIS_OUT);
                using (var dbContext = new DBContext(connectType))
                {
                    using (var command = dbContext.CreateCommand("[tiler].[UpdateObjectTypeRecordOnSuccess]"))
                    {
                        command.AddParameter("@objectTypeID", SqlDbType.Int, Renderer.ObjectTypeID);
                        command.AddParameter("@geoSectionObjectID", SqlDbType.Int, Renderer.GeoSectionObjectID);
                        command.AddParameter("@updateDateUTC", SqlDbType.DateTime, updateDateUTC);
                        command.AddParameter("@updateDateLocal", SqlDbType.DateTime, updateDateLocal);
                        command.AddParameter("@runID", SqlDbType.Int, LogManager.RunID);

                        SqlParameter resParameter = new SqlParameter("@res", SqlDbType.Int);
                        resParameter.Direction = ParameterDirection.ReturnValue;

                        command.Parameters.Add(resParameter);

                        command.ExecuteNonQuery();

                        res = (Int32)resParameter.Value > 0;
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.LogCommon("Ошибка при вызове хранимой процедуры [tiler].[UpdateObjectTypeRecordOnSuccess].", ex.Message, ActionTypeEnum.SohranenieResultata, SeverityEnum.Error);
                res = false;
            }
            return res;
        }
        public bool UpdateObjectTypeRecord()
        {
            bool res = false;

            try
            {
                var connectType = new DBConnectionType(Renderer.tilerConfig.WebGIS_OUT);
                using (var dbContext = new DBContext(connectType))
                {
                    using (var command = dbContext.CreateCommand("[tiler].[UpdateObjectTypeRecord]"))
                    {
                        command.AddParameter("@objectTypeID", SqlDbType.Int, Renderer.ObjectTypeID);
                        command.AddParameter("@geoSectionObjectID", SqlDbType.Int, Renderer.GeoSectionObjectID);
                        res = command.ExecuteNonQuery() > 0;
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.LogCommon("Ошибка при вызове хранимой процедуры [tiler].[UpdateObjectTypeRecord].", ex.Message, ActionTypeEnum.Obshee, SeverityEnum.Error);
                res = false;
            }

            return res;
        }
        #endregion
    }
}

﻿using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Timers;
using WebGIS.Core.DatabaseCore;
using WebGIS.Tiler.DL.Enums;
using WebGIS.Tiler.Logging;
using WebGIS.Tiler.Renderers;
using WebGIS.Tiler.Repository;
using WebGIS.TileRendering.Conversions;
 
namespace WebGIS.Tiler
{
    class Program
    {
        #region data
        const int NUM_ATTEMPTS_BEFORE_UPDATE_GENERATION_TIME = 20;
        const int SLEEP_PERIOD_BETWEEN_ATTEMPTS_TO_UPDATE_GENERATION_TIME_SECONDS = 60;
        const int TIMER_UPDATE_PERIOD_MINUTES = 1;

        public static TilerRepository repository = new TilerRepository();

        static Timer activityLogTimer;
        static DateTime updateDateUTC = DateTime.UtcNow;
        static DateTime updateDateLocal = DateTime.Now;
        static int workingIndex = -1;
        static List<ModifiedTypesInfo> modifiedTypesInfo = null;
        #endregion

        static void Main(string[] args)
        {
            LogManager.LogCommon("Старт.", null, ActionTypeEnum.Obshee, SeverityEnum.Common);
            //Удаляем логи, старше недели
            LogManager.DeleteOldLog();

            if (IsInstanceExist())
            {
                LogManager.LogCommon("Приложение уже запущено.", null, ActionTypeEnum.Obshee, SeverityEnum.Common);
                return;
            }

            if (repository == null)
                return;

            try
            {
                #region config
                LogManager.LogCommon("Загружаем конфигурацию.", null, ActionTypeEnum.Obshee, SeverityEnum.Common);
                Renderer.tilerConfig = TilerConfig.Read();

                if (!Directory.Exists(Renderer.tilerConfig.OutputFolder))
                    Directory.CreateDirectory(Renderer.tilerConfig.OutputFolder);

                Renderer.MaxActiveThreads = Renderer.tilerConfig.MaxConnectionsCount;
                Renderer.semaphorHandle = new System.Threading.SemaphoreSlim(Renderer.tilerConfig.MaxWorkingThreads, Renderer.tilerConfig.MaxWorkingThreads);
                Renderer.AffinityMask = (byte)Renderer.tilerConfig.AffinityMask;
                #endregion

                ProcessThread pt = Process.GetCurrentProcess().Threads.OfType<ProcessThread>().Single(pt_ => pt_.Id == Renderer.GetCurrentThreadId());
                pt.ProcessorAffinity = (IntPtr)Renderer.AffinityMask;

                LogManager.LogCommon("Получаем все типы объектов, в которых есть изменённые объекты.", null, ActionTypeEnum.Obshee, SeverityEnum.Common);

                int RunID = 0;
                modifiedTypesInfo = repository.GetModifiedTypes(out RunID);

                LogManager.RunID = RunID;

                for (var i = 0; i < modifiedTypesInfo.Count; i++)
                {
                    Renderer.ObjectTypeID = modifiedTypesInfo[i].ObjectTypeID;
                    Renderer.GeometryType = (GeometryTypeEnum)modifiedTypesInfo[i].GeometryTypeID;
                    Renderer.LinkedRasterGeoSectionObjectID = modifiedTypesInfo[i].LinkedRasterGeoSectionObjectID;
                    Renderer.ObjectTypeSysName = modifiedTypesInfo[i].ObjectTypeSysName;
                    Renderer.GeoSectionObjectID = modifiedTypesInfo[i].GeoSectionObjectID;

                    LogManager.LogCommon("Если был незавершенный запуск, увеличиваем счётчик ошибок.", null, ActionTypeEnum.Obshee, SeverityEnum.Common);
                    var res = repository.IncrementErrorCounts(Renderer.ObjectTypeID, Renderer.GeoSectionObjectID);

                    if (!res)
                        continue;

                    LogManager.LogCommon("Получаем стили для типа объекта.", null, ActionTypeEnum.Obshee, SeverityEnum.Common);
                    Renderer.colorState = repository.GetColorStates(Renderer.ObjectTypeID, Renderer.GeometryType);

                    bool isValidColorState = Validator.ValidateColorState(Renderer.colorState, Renderer.GeometryType);
                    if (!isValidColorState)
                    {
                        rollback();
                        return;
                    }

                    TilerRepository.PrepareFailedQuadkeys(Renderer.ObjectTypeID, Renderer.GeoSectionObjectID);

                    LogManager.LogCommon("Запускаем таймер на обновление.", null, ActionTypeEnum.Obshee, SeverityEnum.Common);
                    activityLogTimer = new Timer(TIMER_UPDATE_PERIOD_MINUTES * 60 * 1000);
                    activityLogTimer.Elapsed += activityLogTimer_Elapsed;
                    activityLogTimer.Start();
                    GC.KeepAlive(activityLogTimer);

                    repository.UpdateObjectTypeRecord();

                    workingIndex = i;
                    break;
                }

                if (workingIndex == -1)
                {
                    rollback();
                    return;
                }


                if (!Directory.Exists(Path.Combine(Renderer.tilerConfig.OutputFolder, Renderer.ObjectTypeSysName)))
                    Directory.CreateDirectory(Path.Combine(Renderer.tilerConfig.OutputFolder, Renderer.ObjectTypeSysName));

                if (!Directory.Exists(Path.Combine(Renderer.tilerConfig.OutputFolder, Renderer.ObjectTypeSysName, string.Format("gso{0}", Renderer.LinkedRasterGeoSectionObjectID))))
                    Directory.CreateDirectory(Path.Combine(Renderer.tilerConfig.OutputFolder, Renderer.ObjectTypeSysName, string.Format("gso{0}", Renderer.LinkedRasterGeoSectionObjectID)));


                bool getOnlyFailedQuadkeys = false;
                if (!modifiedTypesInfo[workingIndex].LastGenerationTimeUTC.HasValue)
                {
                    if (modifiedTypesInfo[workingIndex].ErrorCount > 0 &&
                        modifiedTypesInfo[workingIndex].AllQuadkeysWasProceededOnLastRun)
                    {
                        getOnlyFailedQuadkeys = true;
                    }
                    else
                    {
                        LogManager.LogCommon("Удаляем копию тайлов предыдущей генерации, если таковая была.", null, ActionTypeEnum.Obshee, SeverityEnum.Common);
                        recursionDelete(Path.Combine(Renderer.tilerConfig.OutputFolder, Renderer.ObjectTypeSysName, string.Format("gso{0}", Renderer.LinkedRasterGeoSectionObjectID)), true);
                    }
                }

                switch (Renderer.GeometryType)
                {
                    #region Point
                    case GeometryTypeEnum.Point:

                        //необходимо рассмотреть случай, когда предыдущая генерация для пары (ObjectTypeID, GeoSectionObjectID) была с ошибкой для tileX, tileY, а когда начали генерацию следующий раз
                        //объект который находился на этом месте изменился (удалился). 
                        //Если модифицированных объектов нет, тогда просто перегенерируем сбойные квадкеи.
                        //Если модифицированные объекты есть, тогда необходимо перегенерировать квадкеи для модифицированных/удаленных объектов в объединении со сбойными квадкеями.  (объединение необходимо проводить на сервере)
                        //Т.е хранимка всегда возвращает список квадкеев которые необходимо перегенерить

                        //Tiler запускается по расписанию, обрабатываем сбойные тайлы для пар (ObjectTypeID, GeoSectionObjectID) - MaxErrorCount раз, далее переходим к следующей по списку паре.
                        for (int z = Renderer.tilerConfig.MinZoom; z <= Renderer.tilerConfig.MaxZoom; z++)
                        {
                            if ((modifiedTypesInfo[workingIndex].ZoomBitMask & (1 << z)) != 0)
                            {
                                if (!Directory.Exists(Path.Combine(Renderer.tilerConfig.OutputFolder, Renderer.ObjectTypeSysName, string.Format("gso{0}", Renderer.LinkedRasterGeoSectionObjectID))))
                                    Directory.CreateDirectory(Path.Combine(Renderer.tilerConfig.OutputFolder, Renderer.ObjectTypeSysName, string.Format("gso{0}", Renderer.LinkedRasterGeoSectionObjectID)));

                                try
                                {
                                    using (SqlConnection conn = new SqlConnection(Renderer.tilerConfig.WebGIS_IN))
                                    {
                                        LogManager.LogCommon(string.Format("Получаем список quadkeys для точек на масштабе {0}.", z), null, ActionTypeEnum.Obshee, SeverityEnum.Common);

                                        SqlCommand cmd = new SqlCommand("[tiler].[GetQuadkeys]", conn) { CommandTimeout = 720, CommandType = CommandType.StoredProcedure };
                                        cmd.Parameters.Add(new SqlParameter("@objectTypeID", SqlDbType.Int) { Value = Renderer.ObjectTypeID });
                                        cmd.Parameters.Add(new SqlParameter("@geoSectionObjectID", SqlDbType.Int) { Value = Renderer.GeoSectionObjectID });
                                        cmd.Parameters.Add(new SqlParameter("@zoom", SqlDbType.Int) { Value = z });
                                        cmd.Parameters.Add(new SqlParameter("@lastUpdateTimeUTC", SqlDbType.DateTime) { Value = modifiedTypesInfo[workingIndex].LastGenerationTimeUTC.HasValue ? (object)modifiedTypesInfo[workingIndex].LastGenerationTimeUTC.Value : null });
                                        cmd.Parameters.Add(new SqlParameter("@mesh_level", SqlDbType.Int) { Value = 0 });
                                        cmd.Parameters.Add(new SqlParameter("@onlyFailedQuadkeys", SqlDbType.Bit) { Value = getOnlyFailedQuadkeys });

                                        bool isCluster = (modifiedTypesInfo[workingIndex].ClusterBitMask & (1 << z)) != 0;
                                        cmd.Parameters.Add(new SqlParameter("@isCluster", SqlDbType.Bit) { Value = isCluster });

                                        conn.Open();
                                        var reader = cmd.ExecuteReader();

                                        if (reader.HasRows)
                                            processQuadkeys(reader, z, GeometryTypeEnum.Point, AverageSizeEnum.Small, isCluster);

                                        reader.Dispose();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogManager.LogCommon("Ошибка при вызове хранимой процедуры [tiler].[GetQuadkeys].", ex.Message, ActionTypeEnum.PoluchenieDannih, SeverityEnum.Error);
                                    rollback();
                                    return;
                                }
                            }
                        }
                        break;
                    #endregion
                    #region Polyline
                    case GeometryTypeEnum.Polyline:
                        for (int z = Renderer.tilerConfig.MinZoom; z <= Renderer.tilerConfig.MaxZoom; z++)
                        {
                            if ((modifiedTypesInfo[workingIndex].ZoomBitMask & (1 << z)) != 0)
                            {
                                if (!Directory.Exists(Path.Combine(Renderer.tilerConfig.OutputFolder, Renderer.ObjectTypeSysName, string.Format("gso{0}", Renderer.LinkedRasterGeoSectionObjectID))))
                                    Directory.CreateDirectory(Path.Combine(Renderer.tilerConfig.OutputFolder, Renderer.ObjectTypeSysName, string.Format("gso{0}", Renderer.LinkedRasterGeoSectionObjectID)));

                                try
                                {
                                    using (SqlConnection conn = new SqlConnection(Renderer.tilerConfig.WebGIS_IN))
                                    {
                                        LogManager.LogCommon(string.Format("Получаем список quadkeys для полилиний на масштабе {0}.", z), null, ActionTypeEnum.Obshee, SeverityEnum.Common);

                                        SqlCommand cmd = new SqlCommand("[tiler].[GetQuadkeys]", conn) { CommandTimeout = 720, CommandType = CommandType.StoredProcedure };
                                        cmd.Parameters.Add(new SqlParameter("@objectTypeID", SqlDbType.Int) { Value = Renderer.ObjectTypeID });
                                        cmd.Parameters.Add(new SqlParameter("@geoSectionObjectID", SqlDbType.Int) { Value = Renderer.GeoSectionObjectID });
                                        cmd.Parameters.Add(new SqlParameter("@zoom", SqlDbType.Int) { Value = z });
                                        cmd.Parameters.Add(new SqlParameter("@lastUpdateTimeUTC", SqlDbType.DateTime) { Value = modifiedTypesInfo[workingIndex].LastGenerationTimeUTC.HasValue ? (object)modifiedTypesInfo[workingIndex].LastGenerationTimeUTC.Value : null });
                                        cmd.Parameters.Add(new SqlParameter("@mesh_level", SqlDbType.Int) { Value = 0 });
                                        cmd.Parameters.Add(new SqlParameter("@onlyFailedQuadkeys", SqlDbType.Bit) { Value = getOnlyFailedQuadkeys });

                                        conn.Open();
                                        var reader = cmd.ExecuteReader();

                                        if (reader.HasRows)
                                            processQuadkeys(reader, z, GeometryTypeEnum.Polyline, AverageSizeEnum.Small);

                                        reader.Dispose();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogManager.LogCommon("Ошибка при вызове хранимой процедуры [tiler].[GetQuadkeys].", ex.Message, ActionTypeEnum.PoluchenieDannih, SeverityEnum.Error);
                                    rollback();
                                    return;
                                }
                            }
                        }
                        break;
                    #endregion
                    #region Sector, Polygon
                    case GeometryTypeEnum.Sector:
                    case GeometryTypeEnum.Polygon:
                        for (int z = Renderer.tilerConfig.MinZoom; z <= Renderer.tilerConfig.MaxZoom; z++)
                        {
                            if ((modifiedTypesInfo[workingIndex].ZoomBitMask & (1 << z)) != 0)
                            {
                                if (!Directory.Exists(Path.Combine(Renderer.tilerConfig.OutputFolder, Renderer.ObjectTypeSysName, string.Format("gso{0}", Renderer.LinkedRasterGeoSectionObjectID))))
                                    Directory.CreateDirectory(Path.Combine(Renderer.tilerConfig.OutputFolder, Renderer.ObjectTypeSysName, string.Format("gso{0}", Renderer.LinkedRasterGeoSectionObjectID)));

                                var avgSizeForZoom = modifiedTypesInfo[workingIndex].AvgSize / Math.Pow(2, (17 - z));
                                if (avgSizeForZoom < Renderer.tilerConfig.BigPolygonSize)
                                {
                                    // small size
                                    try
                                    {
                                        using (SqlConnection conn = new SqlConnection(Renderer.tilerConfig.WebGIS_IN))
                                        {
                                            LogManager.LogCommon(string.Format("Получаем список quadkeys для маленьких секторов/полигонов на масштабе {0}.", z), null, ActionTypeEnum.Obshee, SeverityEnum.Common);

                                            SqlCommand cmd = new SqlCommand("[tiler].[GetQuadkeys]", conn) { CommandTimeout = 720, CommandType = CommandType.StoredProcedure };
                                            cmd.Parameters.Add(new SqlParameter("@objectTypeID", SqlDbType.Int) { Value = Renderer.ObjectTypeID });
                                            cmd.Parameters.Add(new SqlParameter("@geoSectionObjectID", SqlDbType.Int) { Value = Renderer.GeoSectionObjectID });
                                            cmd.Parameters.Add(new SqlParameter("@zoom", SqlDbType.Int) { Value = z });
                                            cmd.Parameters.Add(new SqlParameter("@lastUpdateTimeUTC", SqlDbType.DateTime) { Value = modifiedTypesInfo[workingIndex].LastGenerationTimeUTC.HasValue ? (object)modifiedTypesInfo[workingIndex].LastGenerationTimeUTC.Value : null });
                                            cmd.Parameters.Add(new SqlParameter("@mesh_level", SqlDbType.Int) { Value = 0 });
                                            cmd.Parameters.Add(new SqlParameter("@onlyFailedQuadkeys", SqlDbType.Bit) { Value = getOnlyFailedQuadkeys });

                                            conn.Open();
                                            var reader = cmd.ExecuteReader();

                                            if (reader.HasRows)
                                                processQuadkeys(reader, z, GeometryTypeEnum.Sector, AverageSizeEnum.Small);

                                            reader.Dispose();
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LogManager.LogCommon("Ошибка при вызове хранимой процедуры [tiler].[GetQuadkeys].", ex.Message, ActionTypeEnum.PoluchenieDannih, SeverityEnum.Error);
                                        rollback();
                                        return;
                                    }
                                }
                                else
                                {
                                    // medium size
                                    try
                                    {
                                        using (SqlConnection conn = new SqlConnection(Renderer.tilerConfig.WebGIS_IN))
                                        {
                                            LogManager.LogCommon(string.Format("Получаем список quadkeys для больших секторов/полигонов на масштабе {0}.", z), null, ActionTypeEnum.Obshee, SeverityEnum.Common);

                                            SqlCommand cmd = new SqlCommand("[tiler].[GetQuadkeys]", conn) { CommandTimeout = 720, CommandType = CommandType.StoredProcedure };
                                            cmd.Parameters.Add(new SqlParameter("@objectTypeID", SqlDbType.Int) { Value = Renderer.ObjectTypeID });
                                            cmd.Parameters.Add(new SqlParameter("@geoSectionObjectID", SqlDbType.Int) { Value = Renderer.GeoSectionObjectID });
                                            cmd.Parameters.Add(new SqlParameter("@zoom", SqlDbType.Int) { Value = z });
                                            cmd.Parameters.Add(new SqlParameter("@lastUpdateTimeUTC", SqlDbType.DateTime) { Value = modifiedTypesInfo[workingIndex].LastGenerationTimeUTC.HasValue ? (object)modifiedTypesInfo[workingIndex].LastGenerationTimeUTC.Value : null });
                                            cmd.Parameters.Add(new SqlParameter("@mesh_level", SqlDbType.Int) { Value = Renderer.tilerConfig.MeshLevel });
                                            cmd.Parameters.Add(new SqlParameter("@onlyFailedQuadkeys", SqlDbType.Bit) { Value = getOnlyFailedQuadkeys });

                                            conn.Open();
                                            var reader = cmd.ExecuteReader();

                                            if (reader.HasRows)
                                                processQuadkeys(reader, z, GeometryTypeEnum.Sector, AverageSizeEnum.Medium);

                                            reader.Dispose();
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LogManager.LogCommon("Ошибка при вызове хранимой процедуры [tiler].[GetQuadkeys].", ex.Message, ActionTypeEnum.PoluchenieDannih, SeverityEnum.Error);
                                        rollback();
                                        return;
                                    }
                                }
                            }
                        }
                        break;
                    #endregion
                    #region Multipolygon
                    case GeometryTypeEnum.Multipolygon:
                        Renderer.tilerConfig.BunchSize = 0;

                        for (int z = Renderer.tilerConfig.MinZoom; z <= Renderer.tilerConfig.MaxZoom; z++)
                        {

                            if ((modifiedTypesInfo[workingIndex].ZoomBitMask & (1 << z)) != 0)
                            {
                                if (!Directory.Exists(Path.Combine(Renderer.tilerConfig.OutputFolder, Renderer.ObjectTypeSysName, string.Format("gso{0}", Renderer.LinkedRasterGeoSectionObjectID))))
                                    Directory.CreateDirectory(Path.Combine(Renderer.tilerConfig.OutputFolder, Renderer.ObjectTypeSysName, string.Format("gso{0}", Renderer.LinkedRasterGeoSectionObjectID)));

                                var avgSizeForZoom = modifiedTypesInfo[workingIndex].AvgSize / Math.Pow(2, (17 - z));
                                if (avgSizeForZoom < Renderer.tilerConfig.BigPolygonSize)
                                {
                                    // small size
                                    try
                                    {
                                        using (SqlConnection conn = new SqlConnection(Renderer.tilerConfig.WebGIS_IN))
                                        {
                                            LogManager.LogCommon(string.Format("Получаем список quadkeys для маленьких(очень) мультиполигонов на масштабе {0}.", z), null, ActionTypeEnum.Obshee, SeverityEnum.Common);

                                            SqlCommand cmd = new SqlCommand("[tiler].[GetQuadkeys]", conn) { CommandTimeout = 720, CommandType = CommandType.StoredProcedure };
                                            cmd.Parameters.Add(new SqlParameter("@objectTypeID", SqlDbType.Int) { Value = Renderer.ObjectTypeID });
                                            cmd.Parameters.Add(new SqlParameter("@geoSectionObjectID", SqlDbType.Int) { Value = Renderer.GeoSectionObjectID });
                                            cmd.Parameters.Add(new SqlParameter("@zoom", SqlDbType.Int) { Value = z });
                                            cmd.Parameters.Add(new SqlParameter("@lastUpdateTimeUTC", SqlDbType.DateTime) { Value = modifiedTypesInfo[workingIndex].LastGenerationTimeUTC.HasValue ? (object)modifiedTypesInfo[workingIndex].LastGenerationTimeUTC.Value : null });
                                            cmd.Parameters.Add(new SqlParameter("@mesh_level", SqlDbType.Int) { Value = 1 });
                                            cmd.Parameters.Add(new SqlParameter("@onlyFailedQuadkeys", SqlDbType.Bit) { Value = getOnlyFailedQuadkeys });

                                            bool isCluster = (modifiedTypesInfo[workingIndex].ClusterBitMask & (1 << z)) != 0;
                                            cmd.Parameters.Add(new SqlParameter("@isCluster", SqlDbType.Bit) { Value = isCluster });

                                            conn.Open();
                                            var reader = cmd.ExecuteReader();

                                            if (reader.HasRows)
                                                processQuadkeys(reader, z, GeometryTypeEnum.Multipolygon, AverageSizeEnum.Small, isCluster);

                                            reader.Dispose();
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LogManager.LogCommon("Ошибка при вызове хранимой процедуры [tiler].[GetQuadkeys].", ex.Message, ActionTypeEnum.PoluchenieDannih, SeverityEnum.Error);
                                        rollback();
                                        return;
                                    }
                                }
                                else if (avgSizeForZoom >= Renderer.tilerConfig.BigPolygonSize && avgSizeForZoom < Renderer.tilerConfig.BigMultipolygonSize)
                                {
                                    //medium size
                                    try
                                    {
                                        using (SqlConnection conn = new SqlConnection(Renderer.tilerConfig.WebGIS_IN))
                                        {
                                            LogManager.LogCommon(string.Format("Получаем список quadkeys для маленьких мультиполигонов на масштабе {0}.", z), null, ActionTypeEnum.Obshee, SeverityEnum.Common);

                                            SqlCommand cmd = new SqlCommand("[tiler].[GetQuadkeys]", conn) { CommandTimeout = 720, CommandType = CommandType.StoredProcedure };
                                            cmd.Parameters.Add(new SqlParameter("@objectTypeID", SqlDbType.Int) { Value = Renderer.ObjectTypeID });
                                            cmd.Parameters.Add(new SqlParameter("@geoSectionObjectID", SqlDbType.Int) { Value = Renderer.GeoSectionObjectID });
                                            cmd.Parameters.Add(new SqlParameter("@zoom", SqlDbType.Int) { Value = z });
                                            cmd.Parameters.Add(new SqlParameter("@lastUpdateTimeUTC", SqlDbType.DateTime) { Value = modifiedTypesInfo[workingIndex].LastGenerationTimeUTC.HasValue ? (object)modifiedTypesInfo[workingIndex].LastGenerationTimeUTC.Value : null });
                                            cmd.Parameters.Add(new SqlParameter("@mesh_level", SqlDbType.Int) { Value = Renderer.tilerConfig.MeshLevel });
                                            cmd.Parameters.Add(new SqlParameter("@onlyFailedQuadkeys", SqlDbType.Bit) { Value = getOnlyFailedQuadkeys });

                                            bool isCluster = (modifiedTypesInfo[workingIndex].ClusterBitMask & (1 << z)) != 0;
                                            cmd.Parameters.Add(new SqlParameter("@isCluster", SqlDbType.Bit) { Value = isCluster });

                                            conn.Open();
                                            var reader = cmd.ExecuteReader();

                                            if (reader.HasRows)
                                                processQuadkeys(reader, z, GeometryTypeEnum.Multipolygon, AverageSizeEnum.Medium, isCluster);

                                            reader.Dispose();
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LogManager.LogCommon("Ошибка при вызове хранимой процедуры [tiler].[GetQuadkeys].", ex.Message, ActionTypeEnum.PoluchenieDannih, SeverityEnum.Error);
                                        rollback();
                                        return;
                                    }
                                }
                                else
                                {
                                    //big size
                                    string quadkey = string.Empty;
                                    int leftTilePos = 0, rightTilePos = 0, topTilePos = 0, bottomTilePos = 0;

                                    using (SqlConnection conn = new SqlConnection(Renderer.tilerConfig.WebGIS_IN))
                                    {
                                        LogManager.LogCommon("Получаем граничные координаты для больших мультиполигонов.", null, ActionTypeEnum.Obshee, SeverityEnum.Common);

                                        SqlCommand cmd = new SqlCommand("[tiler].[GetQuadkeys]", conn) { CommandTimeout = 720, CommandType = CommandType.StoredProcedure };
                                        cmd.Parameters.Add(new SqlParameter("@objectTypeID", SqlDbType.Int) { Value = Renderer.ObjectTypeID });
                                        cmd.Parameters.Add(new SqlParameter("@geoSectionObjectID", SqlDbType.Int) { Value = Renderer.GeoSectionObjectID });
                                        cmd.Parameters.Add(new SqlParameter("@zoom", SqlDbType.Int) { Value = z });
                                        cmd.Parameters.Add(new SqlParameter("@lastUpdateTimeUTC", SqlDbType.DateTime) { Value = modifiedTypesInfo[workingIndex].LastGenerationTimeUTC });
                                        cmd.Parameters.Add(new SqlParameter("@mesh_level", SqlDbType.Int) { Value = 0 });

                                        conn.Open();
                                        var tables = cmd.GetDataSet().Tables;

                                        using (DataTableReader reader = tables[1].CreateDataReader())
                                        {
                                            if (reader.HasRows)
                                            {
                                                while (reader.Read())
                                                {
                                                    SqlGeometry envelope = (SqlGeometry)reader["GeoData"];
                                                    if (z - Renderer.tilerConfig.MeshLevel > 5)
                                                    {
                                                        leftTilePos = Helpers.getXTilePos(envelope.STPointN(1).STX.Value, z - Renderer.tilerConfig.MeshLevel);
                                                        rightTilePos = Helpers.getXTilePos(envelope.STPointN(3).STX.Value, z - Renderer.tilerConfig.MeshLevel);
                                                        topTilePos = Helpers.getYTilePos(envelope.STPointN(1).STY.Value, z - Renderer.tilerConfig.MeshLevel);
                                                        bottomTilePos = Helpers.getYTilePos(envelope.STPointN(3).STY.Value, z - Renderer.tilerConfig.MeshLevel);

                                                        int min = Math.Min(leftTilePos, rightTilePos);
                                                        int max = Math.Max(leftTilePos, rightTilePos);
                                                        leftTilePos = min;
                                                        rightTilePos = max;

                                                        min = Math.Min(topTilePos, bottomTilePos);
                                                        max = Math.Max(topTilePos, bottomTilePos);
                                                        topTilePos = min;
                                                        bottomTilePos = max;
                                                    }
                                                    else
                                                    {
                                                        leftTilePos = Helpers.getXTilePos(envelope.STPointN(1).STX.Value, z);
                                                        rightTilePos = Helpers.getXTilePos(envelope.STPointN(3).STX.Value, z);
                                                        topTilePos = Helpers.getYTilePos(envelope.STPointN(1).STY.Value, z);
                                                        bottomTilePos = Helpers.getYTilePos(envelope.STPointN(3).STY.Value, z);

                                                        int min = Math.Min(leftTilePos, rightTilePos);
                                                        int max = Math.Max(leftTilePos, rightTilePos);
                                                        leftTilePos = Math.Min(min - 1, 0);
                                                        rightTilePos = max + 1;

                                                        min = Math.Min(topTilePos, bottomTilePos);
                                                        max = Math.Max(topTilePos, bottomTilePos);
                                                        topTilePos = Math.Min(min - 1, 0);
                                                        bottomTilePos = max + 1;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    int i = leftTilePos;
                                    int j = topTilePos;
                                    while (true)
                                    {
                                        if (Renderer.ActiveThreads >= Renderer.MaxActiveThreads)
                                        {
                                            Renderer.eventHandle.WaitOne();
                                        }
                                        else
                                        {
                                            if (j >= bottomTilePos)
                                            {
                                                j = topTilePos;
                                                i++;
                                            }

                                            if (i > rightTilePos)
                                                break;

                                            quadkey = string.Empty;
                                            if (z - Renderer.tilerConfig.MeshLevel > 5)
                                                quadkey = Helpers.TileXYToQuadKey(i, j, z - Renderer.tilerConfig.MeshLevel);
                                            else
                                                quadkey = Helpers.TileXYToQuadKey(i, j, z);

                                            int zoom = z;
                                            System.Threading.Thread t = new System.Threading.Thread(() => BigMultipolygonRenderer.ThreadProcMultipolygon(quadkey, zoom));
                                            t.Start();

                                            Renderer.ActiveThreads++;
                                            j++;
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    #endregion
                    default: break;
                }
            }
            catch (Exception ex)
            {
                LogManager.LogCommon("Во время выполнения main произошла ошибка.", ex.Message, ActionTypeEnum.Obshee, SeverityEnum.Error);
                rollback();
                return;
            }
            

            int attemps = 0;
            while (true)
            {
                //Console.WriteLine("Ожидаем завершения потоков рендеринга 10 секунд.");
                LogManager.LogCommon("Ожидаем завершения потоков рендеринга 60 секунд.", null, ActionTypeEnum.Obshee, SeverityEnum.Common);
                //проверяем NUM_ATTEMPTS_BEFORE_UPDATE_GENERATION_TIME раз, закончился ли расчёт тайлов
                if (attemps > NUM_ATTEMPTS_BEFORE_UPDATE_GENERATION_TIME)
                    break;

                if (Renderer.ActiveThreads == 0 && Renderer.semaphorHandle.CurrentCount == Renderer.tilerConfig.MaxWorkingThreads)
                    break;
                else
                    Renderer.updateObjectTypeRecordEventHandle.WaitOne(SLEEP_PERIOD_BETWEEN_ATTEMPTS_TO_UPDATE_GENERATION_TIME_SECONDS * 1000);

                attemps++;
            }

            commit();

            //int failedQuadkeysNum = TilerRepository.GetFailedQuadkeys(Renderer.ObjectTypeID, Renderer.GeoSectionObjectID);

            bool update = false;
            //if ((Renderer.ActiveThreads == 0 && Renderer.semaphorHandle.CurrentCount == Renderer.tilerConfig.MaxWorkingThreads) ||
            //    //по каким-то причинам не все потоки освобидились, тогда проверяем были ли сбойные квадкеи
            //    failedQuadkeysNum == 0)
            //{
                LogManager.LogCommon("Обновляем дату последней успешной генерации.", null, ActionTypeEnum.Obshee, SeverityEnum.Common);
                update = repository.UpdateObjectTypeRecordOnSuccess(updateDateUTC, updateDateLocal);
            //}

            if (update)
            {
                LogManager.LogCommon("Генерация тайлов завершёна успешно.", null, ActionTypeEnum.Obshee, SeverityEnum.Common);
            }
            else
            {
                LogManager.LogCommon("- вчера оно работало, а сегодня не работает, это – Виндоус…", null, ActionTypeEnum.Obshee, SeverityEnum.Common);
            }
        }

        #region private
        static void rollback()
        {
            TilerRepository.DeleteFixedQuadkeys(Renderer.ObjectTypeID, Renderer.GeoSectionObjectID);

            LogManager.LogCommon("Останавливаем таймер обновления состояния генерации тайлов.", null, ActionTypeEnum.Obshee, SeverityEnum.Common);
            if (activityLogTimer != null && activityLogTimer.Enabled)
                activityLogTimer.Stop();
        }
        static void commit()
        {
            TilerRepository.DeleteFixedQuadkeys(Renderer.ObjectTypeID, Renderer.GeoSectionObjectID);

            LogManager.LogCommon("Останавливаем таймер обновления состояния генерации тайлов.", null, ActionTypeEnum.Obshee, SeverityEnum.Common);
            if (activityLogTimer != null && activityLogTimer.Enabled)
                activityLogTimer.Stop();
        }
        static void recursionDelete(string path, bool doesDeleteParent = true)
        {
            DirectoryInfo parentDir = new DirectoryInfo(path);

            if (!parentDir.Exists)
                return;

            foreach (FileInfo file in parentDir.GetFiles())
                file.Delete();

            foreach (DirectoryInfo subDir in parentDir.GetDirectories())
                subDir.Delete(true);

            if (doesDeleteParent)
                parentDir.Delete(true);
        }
        static void activityLogTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                if (repository != null)
                {
                    bool res = repository.UpdateObjectTypeRecord();
                    if (!res)
                    {
                        if (activityLogTimer != null && activityLogTimer.Enabled)
                            activityLogTimer.Stop();
                        Environment.Exit(1);
                    }
                    else
                    {
                        //LogManager.LogObject("Обновление записи в очереди.", null, LogManager.QueueRecordID.QueueID, LogManager.QueueRecordID.ImportEntityType);
                    }
                }
            });
        }
        static void processQuadkeys(SqlDataReader reader, int z, GeometryTypeEnum geometryType, AverageSizeEnum size, bool isCluster = false)
        {
            string quadkey = string.Empty;
            int tileX, tileY, level;
            List<string> quadkeys = new List<string>();

            while (true)
            {
                if (Renderer.ActiveThreads >= Renderer.MaxActiveThreads)
                {
                    Renderer.eventHandle.WaitOne();
                }
                else
                {
                    bool isEnd = false;
                    try
                    {
                        isEnd = !reader.Read();

                        if (isEnd)
                        {
                            if (size == AverageSizeEnum.Small && quadkeys.Count > 0) 
                            {
                                List<string> th_quadkeys = new List<string>(quadkeys);
                                int th_zoom = z;
                                System.Threading.Thread t;
                                switch (geometryType)
                                {
                                    case GeometryTypeEnum.Point:
                                        t = new System.Threading.Thread(() => PointRenderer.ThreadProcPoint_I(th_quadkeys, th_zoom, isCluster));
                                        t.Start();
                                        break;
                                    case GeometryTypeEnum.Polyline:
                                        t = new System.Threading.Thread(() => PolylineRenderer.ThreadProcPolyline_I(th_quadkeys, th_zoom));
                                        t.Start();
                                        break;
                                    case GeometryTypeEnum.Sector:
                                    case GeometryTypeEnum.Polygon:
                                        t = new System.Threading.Thread(() => SmallSectorRenderer.ThreadProcSector_I(th_quadkeys, th_zoom));
                                        t.Start();
                                        break;
                                    //!!!
                                    case GeometryTypeEnum.Multipolygon:
                                        string th_quadkey = quadkeys[0];
                                        t = new System.Threading.Thread(() => MultipolygonRenderer.ThreadProcMultipolygon(th_quadkey, th_zoom, isCluster));
                                        t.Start();
                                        break;
                                    //!!!
                                    default: break;
                                }
                                quadkeys.Clear();
                                Renderer.ActiveThreads++;
                            }

                            break;
                        }

                        if (reader["Quadkey"] != null)
                        {
                            quadkey = reader["Quadkey"].ToString();
                            //quadkey = "12012120";

                            if (string.IsNullOrEmpty(quadkey) && isEnd)
                                break;
                            else if (string.IsNullOrEmpty(quadkey))
                                continue;

                            Helpers.QuadKeyToTileXY(quadkey, out tileX, out tileY, out level);

                            if (size == AverageSizeEnum.Small)
                            {
                                quadkeys.Add(quadkey);
                                if (quadkeys.Count > Renderer.tilerConfig.BunchSize || (isEnd && quadkeys.Count > 0))
                                {
                                    List<string> th_quadkeys = new List<string>(quadkeys);
                                    int th_zoom = z;
                                    System.Threading.Thread t;
                                    switch (geometryType)
                                    {
                                        case GeometryTypeEnum.Point:
                                            t = new System.Threading.Thread(() => PointRenderer.ThreadProcPoint_I(th_quadkeys, th_zoom, isCluster));
                                            t.Start();
                                            break;
                                        case GeometryTypeEnum.Polyline:
                                            t = new System.Threading.Thread(() => PolylineRenderer.ThreadProcPolyline_I(th_quadkeys, th_zoom));
                                            t.Start();
                                            break;
                                        case GeometryTypeEnum.Sector:
                                        case GeometryTypeEnum.Polygon:
                                            t = new System.Threading.Thread(() => SmallSectorRenderer.ThreadProcSector_I(th_quadkeys, th_zoom));
                                            t.Start();
                                            break;
                                        //!!!
                                        case GeometryTypeEnum.Multipolygon:
                                            string th_quadkey = quadkey;
                                            t = new System.Threading.Thread(() => MultipolygonRenderer.ThreadProcMultipolygon(th_quadkey, th_zoom, isCluster));
                                            t.Start();
                                            break;
                                        //!!!
                                        default: break;
                                    }
                                    quadkeys.Clear();
                                    Renderer.ActiveThreads++;
                                }
                            }
                            else if (size == AverageSizeEnum.Medium)
                            {
                                int th_zoom = z;
                                string th_quadkey = quadkey;
                                System.Threading.Thread t;
                                switch (geometryType)
                                {
                                    case GeometryTypeEnum.Sector:
                                    case GeometryTypeEnum.Polygon:
                                        t = new System.Threading.Thread(() => BigSectorRenderer.ThreadProcPolygon(th_quadkey, th_zoom));
                                        t.Start();
                                        break;
                                    case GeometryTypeEnum.Multipolygon:
                                        t = new System.Threading.Thread(() => MultipolygonRenderer.ThreadProcMultipolygon(th_quadkey, th_zoom, isCluster));
                                        t.Start();
                                        break;
                                    default: break;
                                }
                                Renderer.ActiveThreads++;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        TilerRepository.AddFailedQuadkey(Renderer.ObjectTypeID, Renderer.GeoSectionObjectID, quadkey, z);
                        LogManager.LogError(string.Format("Во время получения списка quadkeys произошла обшибка. {0}", quadkey), ex.Message, quadkey, ActionTypeEnum.PoluchenieDannih, SeverityEnum.Error);

                        if (isEnd)
                            break;
                    }
                }
            }
        }
        static bool IsInstanceExist()
        {
            var processFullName = Assembly.GetEntryAssembly().Location;
            var processList = Process.GetProcessesByName(Path.GetFileNameWithoutExtension(processFullName));

            var result = processList != null && processList.Count(p => p.MainModule != null && p.MainModule.FileName.Equals(processFullName)) > 1;

            return result;
        }
        #endregion
    }
}
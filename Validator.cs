﻿using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebGIS.Tiler.DL.Enums;
using WebGIS.Tiler.Logging;
 
namespace WebGIS.Tiler
{
    public class Validator
    {
        public static bool ValidateColorState(ColorStateInfo colorState, GeometryTypeEnum geometryType)
        {
            if (colorState == null)
            {
                LogManager.LogCommon("ColorState не задан.", null, ActionTypeEnum.Obshee, SeverityEnum.Error);
                return false;
            }

            switch (geometryType)
            {
                case GeometryTypeEnum.Point:
                    if (colorState.PointColorState == null)
                    {
                        LogManager.LogCommon("ColorState для точек не задан.", null, ActionTypeEnum.Obshee, SeverityEnum.Error);
                        return false;
                    }

                    if (colorState.PointClusterColorState == null)
                    {
                        LogManager.LogCommon("ColorState для кластеров не задан.", null, ActionTypeEnum.Obshee, SeverityEnum.Error);
                        return false;
                    }

                    if (colorState.PointColorState.PngData == null || colorState.PointColorState.Width == 0 || colorState.PointColorState.Height == 0)
                    {
                        LogManager.LogCommon("Иконка|ширина|высота для точек не задан.", null, ActionTypeEnum.Obshee, SeverityEnum.Error);
                        return false;
                    }

                    if (colorState.PointClusterColorState.PngData == null || colorState.PointClusterColorState.Width == 0 || colorState.PointClusterColorState.Height == 0)
                    {
                        LogManager.LogCommon("Иконка|ширина|высота для кластеров не задан.", null, ActionTypeEnum.Obshee, SeverityEnum.Error);
                        return false;
                    }
                    break;
                case GeometryTypeEnum.Polyline:
                    if (colorState.PolylineColorStateDict.Count == 0)
                    {
                        LogManager.LogCommon("ColorState для полилиний не задан.", null, ActionTypeEnum.Obshee, SeverityEnum.Error);
                        return false;
                    }

                    //if (string.IsNullOrEmpty(colorState.PolylineColorState.StrokeARGB) || string.IsNullOrEmpty(colorState.PolylineColorState.FillARGB) || string.IsNullOrEmpty(colorState.PolylineColorState.CenterARGB))
                    //{
                    //    LogManager.LogCommon("Цвета контура|заливки|центра для полилиний не заданы.", null, ActionTypeEnum.Obshee, SeverityEnum.Error);
                    //    return false;
                    //}
                    break;
                case GeometryTypeEnum.Sector:
                case GeometryTypeEnum.Polygon:
                case GeometryTypeEnum.Multipolygon:
                    if (colorState.PolygonColorStateDict.Count == 0)
                    {
                        LogManager.LogCommon("ColorState для секторов/полигонов/мультиполигонов не задан.", null, ActionTypeEnum.Obshee, SeverityEnum.Error);
                        return false;
                    }

                    //if (string.IsNullOrEmpty(colorState.PolygonColorState.StrokeARGB) || string.IsNullOrEmpty(colorState.PolygonColorState.FillARGB))
                    //{
                    //    LogManager.LogCommon("Цвета контура|заливки для секторов/полигонов/мультиполигонов не заданы.", null, ActionTypeEnum.Obshee, SeverityEnum.Error);
                    //    return false;
                    //}
                    break;
            }
            return true;
        }
        public static bool ValidatSqlGeometry(SqlGeometry bbox)
        {
            if (bbox == null)
            {
                LogManager.LogCommon("Объект геометрии не задан.", null, ActionTypeEnum.Obshee, SeverityEnum.Error);
                return false;
            }

            if (!bbox.STIsValid())
            {
                LogManager.LogCommon("Геометрия не валидная.", null, ActionTypeEnum.Obshee, SeverityEnum.Warning);
                return false;
            }

            return true;
        }
        public static bool ValidateClusterIndex(int clusterIndex)
        {
            if (clusterIndex > 0 && clusterIndex <= 7)
            {
                return true;
            }
            else
            {
                LogManager.LogCommon("Используемый уровень кластеризации не валидный.", null, ActionTypeEnum.Obshee, SeverityEnum.Error);
                return false;
            }
        }
        //public static bool ValidatePoints(ModifiedObjectsInfo<PointInfo> modifiedPointsInfo)
        //{
        //    if (modifiedPointsInfo == null)
        //    {
        //        LogManager.LogCommon("Объект содержащий информация о изменённых точечных объектах не задан.", null, ActionTypeEnum.Obshee, SeverityEnum.Error);
        //        return false;
        //    }

        //    if (modifiedPointsInfo.Items == null)
        //    {
        //        LogManager.LogCommon("Список изменённых точечных объектов не задан.", null, ActionTypeEnum.Obshee, SeverityEnum.Error);
        //        return false;
        //    }

        //    if (modifiedPointsInfo.Items.Count == 0)
        //    {
        //        LogManager.LogCommon("Cписок изменённых точечных объектов пустой.", null, ActionTypeEnum.Obshee, SeverityEnum.Warning);
        //        return false;
        //    }
        //    return true;
        //}
        //public static bool ValidateClusters(ModifiedClustersInfo<PointClusterInfo> modifiedClustersInfo, int clusterIndex)
        //{
        //    if (modifiedClustersInfo == null)
        //    {
        //        LogManager.LogCommon("Объект содержащий информация о изменённых кластерах не задан.", null, ActionTypeEnum.Obshee, SeverityEnum.Error);
        //        return false;
        //    }

        //    if (modifiedClustersInfo.Clustered == null)
        //    {
        //        LogManager.LogCommon("Словарь <уровень кластеризации, список кластеров> не задан.", null, ActionTypeEnum.Obshee, SeverityEnum.Error);
        //        return false;
        //    }

        //    if (!modifiedClustersInfo.Clustered.ContainsKey(clusterIndex))
        //    {
        //        LogManager.LogCommon(string.Format("В словаре кластеров не найден список для уровня кластеризации {0}.", clusterIndex), null, ActionTypeEnum.Obshee, SeverityEnum.Warning);
        //        return false;
        //    }

        //    if (modifiedClustersInfo.Clustered[clusterIndex].Count == 0)
        //    {
        //        LogManager.LogCommon(string.Format("Cписок для уровня кластеризации {0} пустой.", clusterIndex), null, ActionTypeEnum.Obshee, SeverityEnum.Warning);
        //        return false;
        //    }
        //    return true;
        //}
        //public static bool ValidatePlylines(ModifiedObjectsInfo<PolylineInfo> modifiedPolylinesInfo)
        //{
        //    if (modifiedPolylinesInfo == null)
        //    {
        //        LogManager.LogCommon("Объект содержащий информация о изменённых полилиниях не задан.", null, ActionTypeEnum.Obshee, SeverityEnum.Error);
        //        return false;
        //    }

        //    if (modifiedPolylinesInfo.Items == null)
        //    {
        //        LogManager.LogCommon("Список изменённых полилиний не задан.", null, ActionTypeEnum.Obshee, SeverityEnum.Error);
        //        return false;
        //    }

        //    if (modifiedPolylinesInfo.Items.Count == 0)
        //    {
        //        LogManager.LogCommon("Cписок изменённых полилиний пустой.", null, ActionTypeEnum.Obshee, SeverityEnum.Warning);
        //        return false;
        //    }
        //    return true;
        //}
        //public static bool ValidatePolygons(ModifiedObjectsInfo<PolygonInfo> modifiedPolygonsInfo)
        //{
        //    if (modifiedPolygonsInfo == null)
        //    {
        //        LogManager.LogCommon("Объект содержащий информация о изменённых секторах/полигонах/мультиполигонах не задан.", null, ActionTypeEnum.Obshee, SeverityEnum.Error);
        //        return false;
        //    }

        //    if (modifiedPolygonsInfo.Items == null)
        //    {
        //        LogManager.LogCommon("Список изменённых секторах/полигонах/мультиполигонах не задан.", null, ActionTypeEnum.Obshee, SeverityEnum.Error);
        //        return false;
        //    }

        //    if (modifiedPolygonsInfo.Items.Count == 0)
        //    {
        //        LogManager.LogCommon("Cписок изменённых секторах/полигонах/мультиполигонах пустой.", null, ActionTypeEnum.Obshee, SeverityEnum.Warning);
        //        return false;
        //    }
        //    return true;
        //}
    }
}
